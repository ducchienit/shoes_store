<?php  
include 'menu.php';
?>
<div class="banner_noidung">
  <h4>Giỏ hàng
</h4>
<div class="lien_ket text-center">
</div>
</div>

<div class="container" style="margin-top: 40px;margin-bottom: 40px;">
  <div class="row">
    <?php if(!empty($cart)) {?>
      <div class="col-md-12">
     <table class="table table-responsive" style="display: inline-table;">
    <thead>
      <tr style="text-align: center">
        <th scope="col">Ảnh</th>
        <th scope="col">Sản phẩm</th>
        <th scope="col">Kích cỡ</th>
        <th scope="col">Màu</th>
        <th scope="col">Giá</th>
        <th scope="col">Số lượng</th>
        <th scope="col">Thành tiền</th>
        <th scope="col">Tác vụ</th>
      </tr>
    </thead>
    <tbody style="border-bottom: 1px solid #dee2e6;">
    <?php foreach ($cart as $key => $value) : ?>
      <tr style="text-align: center">
        <td>
          <a  title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>"><img src="image/<?php echo $value['image']; ?>" alt="" width="100px" height="100px;"></a>
        </td>
        <td><a style="color: #252525;" title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>"><?php echo $value['name']; ?></a></td>
        <td><?php echo $value['size']; ?></td>
        <td><?php echo $value['color']; ?></td>
        <td><?php echo number_format($value['price'],"0",",",".")."₫"; ?></td>
        <td>
          <form action="cart.php">
            <div class="number-input">
						<button type="" onclick="this.parentNode.querySelector('input[name=quantity]').stepDown()" ></button>
            <input type="hidden" name="action" value="update">
            <input type="hidden" name="pro_size_id" value="<?php echo $value['pro_size_id'] ?>">
            <input type="hidden" name="pro_color_id" value="<?php echo $value['pro_color_id'] ?>">
            <input type="number" id="quantity" style="font-size: 18px;" name="quantity" value="<?php echo $value['quantity']; ?>">
						<button type="" onclick="this.parentNode.querySelector('input[name=quantity]').stepUp();let x = document.getElementById('quantity').value;if(x>10){document.getElementById('quantity').value = 10;};" class="plus"></button>
						</div>
          </form>
        </td>
    
        <td><?php echo number_format($value['price'] * $value['quantity'],"0",",",".")."₫" ?></td>
        <td style="text-align: center">
          <a title="Xóa" href="cart.php?pro_size_id=<?php echo $value['pro_size_id']."&pro_color_id=".$value['pro_color_id'] ?>&action=delete" class="btn btn-danger">
          <span><i class="fas fa-times"></i></span>  
        </a>
        </td>
      </tr>
      <?php endforeach ?>
    </tbody>
  </table>
  <div class="tieptucmua row">

        <div class="col-lg-4 col-md-3"></div>
        <div class="col-lg-8 col-md-9" style="text-align: end;">
        <div class="table-total">
              <table class="table">
                <tbody><tr>
                  <td style="border: none;" class="total-text f-left">Tổng tiền</td>
                  <td style="border: none;" class="txt-right totals_price price_end f-right"><?php echo number_format(total_price($cart),"0",",",".")."₫" ?></td>
                </tr>
              </tbody></table>
            </div>
            <a href="home.php" class="tieptuc">Tiếp tục mua hàng</a>
            <?php if(isset($_SESSION['customer']) || isset($_SESSION['admin'])) {?>
              <a href="checkout.php?thanhtoan" class="thanhtoan">Tiến hành thanh toán</a>
            <?php } ?>
            <?php if(!isset($_SESSION['customer']) && !isset($_SESSION['admin'])) {?>
              <a href="login.php" class="thanhtoan">Đăng nhập để thanh toán</a>
						<?php } ?>
        </div>
    
  </div>
     </div>
    <?php }?>
      <?php if(empty($cart)) {?>
        <div class="col-md-12" style="height: 300px;">
        <div class="wrap_background_aside margin-bottom-40">
                <div class="header-cart">
                  <!-- <h1 class="title_cart hidden">
                    <span>Giỏ hàng</span>
                  </h1> -->
                  <div class="header-cart title_cart_pc hidden-sm hidden-xs"><p class="hidden-xs-down">Không có sản phẩm nào. Quay lại <a href="home.php" style="color: #252525;">cửa hàng</a> để tiếp tục mua sắm.</p></div>
                </div>
                <div class="col-main cart_desktop_page cart-page">

      </div>
				</div>
  </div>
      <?php }?> 
  </div>
</div>
<?php  
include 'footerfe.php';
?>