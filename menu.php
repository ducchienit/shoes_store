<?php
ob_start();
session_start();
include 'connect.php';
include 'cart-function.php';
$cart = (isset($_SESSION['cart'])) ? $_SESSION['cart'] : [];
$quantity = null;
foreach($cart as $key => $value){
	$quantity += $value['quantity'];
}
if(isset($_POST['mua'])){
	$size_and_color = $_POST['pro_size_id']+$_POST['pro_color_id'];
	$_POST['quantity'] = ($_POST['quantity'] > 0) ? $_POST['quantity'] : 1;
	$number = (isset($_SESSION['cart'][$size_and_color])) ? ($_SESSION['cart'][$size_and_color]['quantity']) : 0;
	if($number+$_POST['quantity'] > 10){
		$soluong = (isset($_SESSION['cart'][$size_and_color])) ? (10-$_SESSION['cart'][$size_and_color]['quantity']) : 10;
		$quantity += $soluong;
		$_POST['quantity'] = $soluong;
	}else {
		$quantity += (isset($_POST['quantity']) && $_POST['quantity'] > 0) ? $_POST['quantity'] : 1;
	}
}
$customer = (isset($_SESSION['customer'])) ? $_SESSION['customer'] : [];
$admin = (isset($_SESSION['admin'])) ? $_SESSION['admin'] : [];
$cate = "SELECT * FROM categories";
$kq = execQuery($cate);
$bran = "SELECT * FROM brands";
$kq_brand = execQuery($bran);
$styles = "SELECT * FROM styles";
$kq_style = execQuery($styles);
if(isset($_GET['logout'])){
	unset($_SESSION['customer']);
	unset($_SESSION['admin']);
	header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Shop Giày</title>
	<link rel="icon" href="image/logo-giay-dep-07.jpg" />
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/reponsive.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/fontawesome.min.css">
	<link rel="stylesheet" href="OwlCarousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="OwlCarousel/dist/assets/owl.theme.default.css">
	<link rel="stylesheet" href="css/all.css">
	<script src="js/jquery.min.js"></script>
</head>
<body>
	<div class="menu">
		<div class="topnav" style="background:#252525;">
		<div class="container">
			<div class="row">
				<div class="col-md-6 topnav1 d-none d-sm-block">
					<ul>
						<a href="tel:shopgiaytc@gmail.com"><li>shopgiaytc@gmail.com</li></a>
						<li style="color: white">/</li>
						<a href="tel:0359329688"><li>0359329688</li></a>
					</ul>
				</div>
				<div class="col-md-6 topnav2">
					<div class="list-inline">
					
						<ul>
							<?php if(isset($_SESSION['customer']) || isset($_SESSION['admin'])) {?>
								<button title="Tài khoản" class="info" ><?php if (isset($_SESSION['customer'])): ?><?php echo($_SESSION['customer']); ?><?php endif ?><?php if (isset($_SESSION['admin'])): ?><?php echo($_SESSION['admin']); ?><?php endif ?></button>
							<?php } ?>
							<?php if(!isset($_SESSION['customer']) && !isset($_SESSION['admin'])) {?>
								<a title="Đăng ký" class="login" href="regis.php">Đăng ký</a>
							<?php } ?>
							<li style="color: white">/</li>
							<a class="login" title="<?php if(isset($_SESSION['customer']) || isset($_SESSION['admin']))  echo ("Thoát") ?><?php else echo("Đăng nhập") ?>" href="<?php if(isset($_SESSION['customer']) || isset($_SESSION['admin']))  echo ("?logout") ?><?php else echo("login.php") ?>"><?php if(isset($_SESSION['customer']) || isset($_SESSION['admin']))  echo ("Thoát") ?><?php else echo("Đăng nhập") ?></a>
							
							
						</ul>
					</div>
					<?php if(isset($_SESSION['customer']) || isset($_SESSION['admin'])) {?>
					<div class="dropdown-info">
						<a class="info-item" href="thongtin.php">Thông tin tài khoản</a>
						<a class="info-item" href="lich_su_mua.php">Lịch sử mua hàng</a>
						<?php if(isset($_SESSION['admin'])) {?>
						<a class="info-item" href="admin/home">Quản lý trang web</a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<!-- đóng thanh liên hệ -->


	<!-- thanh menu -->	

	<nav style="box-shadow: 0px 0px 5px #252525;padding: 0px;" class="navbar navbar-expand-lg navbar-light d-none  d-sm-block" style="padding: 0px">
		<div class="container">
			<div>
			<a class="navbar-brand" href="home.php">
				<img src="image/logo-giay-dep-07.jpg" alt="" width="100px">
			</a>
			</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="home.php">Trang chủ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " href="gioi-thieu.php">Giới thiệu</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Loại giày
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($kq as $key => $value) { ?>					
									<a class="dropdown-item" href="timsp_dmuc.php?id=<?php echo $value['id']?>"><?php echo $value['name']; ?></a>
								<?php } ?>
							</div>
						</li>
							<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Phong cách
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($kq_style as $key => $value) { ?>					
									<a class="dropdown-item" href="timsp_pc.php?id=<?php echo $value['id']?>"><?php echo $value['name']; ?></a>
								<?php } ?>
							</div>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Thương hiệu
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($kq_brand as $key => $value) { ?>					
									<a class="dropdown-item" href="timsp_th.php?id=<?php echo $value['id']?>"><?php echo $value['name']; ?></a>
								<?php } ?>
							</div>
						</li>
					</ul>
					

					<form class="example" action="timsp.php" style="margin-right: 20px;width: 190px;" method="POST">
						<input style="border-radius: 0;margin: 0px" type="text" placeholder="Tìm kiếm sản phẩm" name="search"> 
						<button title="Tìm kiếm sản phẩm" style="margin: 0px;" type="submit"><i class="fa fa-search"></i></button>
					</form>
					<div style="display: flex;justify-content: inherit;width: 90px;">
					<a style="overflow: visible;padding: 0px;" title="Giỏ hàng" href="view-cart.php" class="cart">
					<i class="fas fa-shopping-cart"></i> <span style="position: relative;left: -20%;top: -50%;" class="badge badge-light"><?php if(!empty($cart) || $quantity > 0) echo($quantity) ?><?php else echo("0") ?></span>
					</a>
					</div>
				</div> 
			</div>
		</nav>

		<!-- menu mobile -->
		<div class="header d-block d-sm-none">			
			<!-- <div class="col-xs-12 col-sm-12 col-lg-12"> -->
				<nav class="navbar navbar-expand-lg navbar-light d-block d-sm-none" style="padding: 0px">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<a class="navbar-brand" href="#"><img src="image/logo-giay-dep-06.jpg" alt="" width="150px" style="margin-left: 5px"></a>
					<i class="fas fa-search seachinff searchmobile"></i>
					<form id="searchbox" method="POST" action="timsp.php" autocomplete="off" style="display: none;margin-left: 60px" class="search-bar">
							<input name="search" type="text" size="15" placeholder="Tìm kiếm sản phẩm" />
							<input id="button-submit" type="submit"/>
					</form>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="background-color: #f72b3f">
								<a class="nav-link" href="#">Trang chủ</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="#" tabindex="-1" aria-disabled="true">Giới thiệu</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Loại giày
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<?php foreach ($kq as $key => $value) { ?>					
										<a class="dropdown-item" href="#"><?php echo $value['name']; ?></a>
									<?php } ?>
								</div>
							</li>
							</li>
							<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Phong cách
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($kq_style as $key => $value) { ?>					
									<a class="dropdown-item" href="#"><?php echo $value['name']; ?></a>
								<?php } ?>
							</div>
						</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Thương hiệu
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<?php foreach ($kq_brand as $key => $value) { ?>					
										<a class="dropdown-item" href="#"><?php echo $value['name']; ?></a>
									<?php } ?>
								</div>
							</li>
    						<span style="margin-left: 4px; font-size: 17px; padding: 15px 10px;font-family: sans-serif"> Giỏ hàng&nbsp;&nbsp; <a href="view-cart.php"><img src=" https://png.pngtree.com/png-clipart/20191122/original/pngtree-cart-icon-isolated-on-abstract-background-png-image_5165752.jpg" alt="" width="45px" style="margin-top: -10px"></a></span>
							<span>
						<!-- <button type="" id="searchmobile"><i class="fa fa-search" style="font-size: 20px;"></i></button> -->
						<!-- nút tì mkieesm -->
						

						<!-- form tìm kiếm -->
						
						
					</span>
						</ul>
					</div> 
				</nav>
				<!-- </div> -->
			</div>
	</div>