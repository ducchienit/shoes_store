<?php 
include 'menu.php';
?>
<div class="banner_noidung">
  <h4>Giới thiệu
</h4>
<div class="lien_ket text-center">
</div>
</div>
<section class="page section_base" style="padding: 40px 0px;">
	<div class="container">
		<div class="wrap_background_aside padding-top-15 margin-bottom-40">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title category-title">
					<h1 class="title-head"><a class="gioithieu" style="color: #252525;" href="gioi-thieu.php">Giới thiệu</a></h1>
				</div>
				<div class="content-page rte">
					<p>Sea Shoes&nbsp;xây dựng một thương hiệu thân thiện thông qua cách chúng tôi làm sản phẩm và đem sản phẩm đó đến tay của bạn. Một sản phẩm thân thiện được cung cấp bởi một dịch vụ thân thiện chính là Kim chỉ nam để thành công của chúng tôi.</p>
<p>Sea Shoes chủ động thay đổi để không ngừng phát triển. Ngoài ra, khách hàng, bằng những sản phẩm của chúng tôi, có thể chủ động trong việc tạo nên phong cách sống riêng biệt trong không gian của mỗi người.</p>
<p>Chúng tôi tin rằng: Không gian sống đẹp có thể làm thay đổi chất lượng cuộc sống của bạn. Và chúng tôi mong mỏi truyền tải niềm tin đó đến cho mọi người để ai cũng có một nơi đáng sống.</p>
<p>Chúng tôi không chỉ cung cấp sản phẩm giày dép chất lượng cao, chúng tôi mang cho bạn phong cách sống. “Less is more” - Một sự đơn giản nhất định nhưng tinh tế trong không gian sống sẽ giúp bạn dễ dàng cân bằng hơn trong cuộc sống</p>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>
<?php 
include 'footerfe.php';
?>