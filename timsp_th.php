<?php  
include 'menu.php';
if(isset($_GET['id'])){
	$id = $_GET['id'];

	$select_category = "SELECT * FROM brands WHERE id = '$id'";
	$result_brand = execQuery($select_category);
	$id_brand = $result_brand[0]['id'];
	$select_product = "SELECT products.*,brands.name as brand FROM products  
	INNER JOIN brands ON products.brand_id = brands.id Where products.brand_id = '$id_brand'  
	AND products.status = 1 ORDER BY products.id";
	$result_products = execQuery($select_product);
}
?>
<div class="banner_noidung">
	<h4 >Sản phẩm theo thương hiệu: <?php echo $result_products[0]['brand'] ?></h4>
</div>

<div class="tim_kiem_sp" style="margin-top: 10px">
	<div class="container">
		<p>Thương hiệu : <strong><?php echo $result_products[0]['brand'] ?></strong></p>
		<?php if (count($result_products) == 0){ ?>
			<?php echo "<span style='color:red'>Không tìm thấy sản phẩm!</span>" ?>
		<?php }else{ ?>
			<div class="row">
				<?php foreach ($result_products as $key => $value): ?>
					<div class="col-md-3 tksp wow zoomInDown" style="margin-top: 20px;height: 363px;">
					<div class="item" style="box-shadow: 0px 0px 5px #ccc;height: 100%;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
								<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
							</a>
							<div class="caption text-center" style="padding: 10px 0px;">
								<div style="font-size: 15px; height: 100px;">
									<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
									<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
									</a>
									<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
									<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand'] ?></p>
									</a>
									<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
								</div>
								<div>
								<a style="padding: 10px 79.8px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
								</div>
							</div>
							</div>
					</div>
				<?php endforeach ?>
			</div>

		<?php } ?>
		
	</div>
</div>

<section class="awe-section-8">	
	<div class="section_hotline lazyload" style="background-image: url(image/guy_shoes_pants_sits.jpg);">
		<div class="parallax">
			<div class="wrapper" style="padding: 80px;">   
				<div class="custom-center text-center">
					<div class="custom-center-body" style="padding: 2px 20px;background: rgba(0, 0, 0, 0.7);">
						<a title="Địa chỉ" target="_blank" class="hai01" href="https://www.google.com/maps/place/238+Ho%C3%A0ng+Qu%E1%BB%91c+Vi%E1%BB%87t,+C%E1%BB%95+Nhu%E1%BA%BF,+C%E1%BA%A7u+Gi%E1%BA%A5y,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/@21.046382,105.7812622,17z/data=!3m1!4b1!4m5!3m4!1s0x3135ab32dd484c53:0x4201b89c8bdfd968!8m2!3d21.046382!4d105.7834509?hl=vi-VN" style="text-decoration: none">Xem địa chỉ shop tại Hoàng Quốc Việt</a>		
					</div>  
				</div>
			</div>    
		</div>
	</div>
</section>
<?php  
include 'footerfe.php';
?>