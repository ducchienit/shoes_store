
<?php  
include 'menu.php';
$name_product = null;
$image_product = null;
$size_product = null;
$color_product = null;
$price_product = null;
if(isset($_POST['mua'])){
	$id= $_POST['id'];
	$soluong = (isset($_POST['quantity']) || $_POST['quantity'] <= 0) ? $_POST['quantity'] : 1;
	$size_and_color = $_POST['pro_size_id']+$_POST['pro_color_id'];
	$pro_size_id = $_POST['pro_size_id'];
	$size = execQuery("SELECT product_sizes.*, sizes.size 
		FROM product_sizes
		INNER JOIN sizes ON product_sizes.size_id = sizes.id
		WHERE product_sizes.id = '$pro_size_id'");
	$pro_color_id = $_POST['pro_color_id'];
	$color = execQuery("SELECT product_colors.*, colors.name as color
		FROM product_colors
		INNER JOIN colors ON product_colors.color_id = colors.id
		WHERE product_colors.id = '$pro_color_id'");
	$product = "SELECT * FROM products WHERE id = '$id'";
	$products = execQuery($product);
	// khai báo mảng để lưu trữ dữ liệu
	$name_product = $products[0]['name'];
	$image_product = $products[0]['image'];
	$size_product = $size[0]['size'];
	$color_product = $color[0]['color'];
	$price_product = $products[0]['price'];
	$item = [
		'id'=>$products[0]['id'],
		'name'=>$products[0]['name'],
		'image'=>$products[0]['image'],
		'price'=>$products[0]['price'],
		'pro_color_id'=>$pro_color_id,
		'pro_size_id'=>$pro_size_id,
		'size'=>$size[0]['size'],
		'color'=>$color[0]['color'],
		'quantity'=> $soluong
	];
	if(isset($_SESSION['cart'][$size_and_color])){
		$_SESSION['cart'][$size_and_color]['quantity'] += $soluong ;
		
	}else{
		// chưa có thì thêm vào giỏ hàng
		$_SESSION['cart'][$size_and_color] = $item;
	}
}
$product = "SELECT * FROM products";
$products = execQuery($product);
if(isset($_GET['sp'])){
	$id = $_GET['sp'];
	$size_check = [];
	$color_check = [];
	$select_product= "SELECT products.*,brands.name as brand,sizes.size,product_sizes.id as pro_size_id  FROM products 
		INNER JOIN brands ON products.brand_id = brands.id 
		INNER JOIN product_sizes ON products.id = product_sizes.product_id
		INNER JOIN sizes ON product_sizes.size_id = sizes.id WHERE products.id = $id AND product_sizes.status = 1 ORDER BY sizes.size ASC";
	$result_product = execQuery($select_product);
	foreach($result_product as $value){
		array_push($size_check,$value['size']);
	}
	$select_color = "SELECT products.*,colors.name as color,product_colors.id as pro_color_id FROM products 
	INNER JOIN product_colors ON products.id = product_colors.product_id
	INNER JOIN colors ON product_colors.color_id = colors.id WHERE products.id = $id AND product_colors.status = 1 ORDER BY colors.name";
	$result_color = execQuery($select_color);
	foreach($result_color as $value){
		array_push($color_check,$value['color']);
	}
	$select_image = "SELECT products.*,product_images.image as imglq FROM products
	INNER JOIN product_images ON products.id = product_images.product_id WHERE products.id = $id";
	$result_image = execQuery($select_image);
	
	$categoryid = $result_product[0]['category_id'];
	$select_category_id = "SELECT  products.*, brands.name as brand 
		FROM products 
		INNER JOIN brands ON products.brand_id = brands.id 
		WHERE products.category_id = '$categoryid' AND products.id != '$id' ORDER BY RAND() LIMIT 3";
	$result_category_id = execQuery($select_category_id);
}	



?>
<!-- banner -->

<div class="banner_noidung">
	<h4><?php echo $result_product[0]['name']; ?>
</h4>
<div class="lien_ket text-center">
	<span style="text-align: center">
		<a href="#" style="text-decoration: none;color: white;font-size: 20px">Thương hiệu / </a><span style="font-size: 20px;color: #f72b3f"><?php echo $result_product[0]['brand']; ?></span>
	</span>
</div>

</div>


<!-- nội dung -->

<div class="content_chitiet">
	<div class="container">
		<form method="POST" action="">
		<div class="row">
			<div class="col-md-6 chitiet wow zoomInLeft">
				<div class="anh_sp_chitiet" >
					<img style="height: 553px;" id="anh-to" src="image/<?php echo $result_product[0]['image']; ?>" alt="" srcset="">
				</div>

				<div class="anh_sp_lq">
					<div class="" style="display: flex;">
						<div class="col-md-3 chitiet_sp_lq">
							<a onclick="changeImageOnClick(event)"><img src="image/<?php echo $result_product[0]['image']; ?>" alt="" srcset="" style="width:100%;height: 110px;" ></a>
						</div>
						<?php foreach ($result_image as $key => $value): ?>			
							<div class="col-md-3 chitiet_sp_lq">
								<a onclick="changeImageOnClick(event)"><img src="image/<?php echo $value['imglq']; ?>" alt="" srcset="" style="width:100%;height: 110px;" ></a>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>

			<div class="col-md-6 noidung_chitiet wow slideInRight">
				
					<h1 class="title-product"><?php echo $result_product[0]['name']; ?></h1>
					<div class="thuonghieu_sp">
						<span>Thương hiệu: <span style="color: green;"><?php echo $result_product[0]['brand']; ?></span></span>
					</div>
						<div>
						<p style="margin-bottom: 0.5rem;">Kích cỡ</p>
						<div class="list-size">
							<?php foreach ($result_product as $key => $value): ?>
								<div class="size-item">
							<input class="size" type="radio" <?php if($value['size'] == $size_check[0]) {?><?php echo("checked='checked'") ?><?php }?> value="<?php echo $value['pro_size_id']; ?>" name="pro_size_id" required="">
									<label class="size-label"><?php echo $value['size']; ?></label>
								</div>
							<?php endforeach ?>
						</div>
						</div>

						<div>
						<p style="margin-bottom: 0.5rem;">Màu</p>
						<div class="list-color">
							<?php foreach ($result_color as $key => $value): ?>	
							<div class="color-item">
								<input class="color" <?php if($value['color'] == $color_check[0]) {?><?php echo("checked='checked'") ?><?php }?> type="radio" value="<?php echo $value['pro_color_id']; ?>" name="pro_color_id" required="">
								<label class="color-label"><?php echo $value['color']; ?></label>
							</div>
							<?php endforeach ?>
						</div>
						</div>
						
					
					<div class="canngang">
						<hr>
					</div>
					<div class="giasanpham">
						<span>Giá sản phẩm: <span class="giasp"><?php echo number_format($result_product[0]['price'],"0",",",".")."₫"; ?></span></span>
					</div>
					<div class="soluongsp row">				
						<div class="number-input col-md-4">
						<button type="button" onclick="this.parentNode.querySelector('input[name=quantity]').stepDown()" ></button>
						<input class="quantity" min="0" id="quantity" name="quantity" value="1" type="number">
							<button type="button" onclick="this.parentNode.querySelector('input[name=quantity]').stepUp();let x = document.getElementById('quantity').value;if(x>10){document.getElementById('quantity').value = 10;};" class="plus"></button>
						</div>
						<div class="col-md-8">
						<input type="hidden" name="id" value="<?php echo $result_product[0]['id']; ?>"> 
						<button title="Mua Ngay" name="mua" style="border: none;padding: 12px 80px;" type="submit" class="mua"><i style="font-size: 18px;color: #fff;" class="fas fa-shopping-cart"></i> Thêm vào giỏ hàng</button>		
						</div>	
					</div>
				<div class="canngang">
					<hr>
				</div>
				<div class="mota_chitiet">
						<p style="font-size: 18px;margin-bottom: 0.5rem;">Mô tả sản phẩm:</p>
						<p style="color: #707070;"><?php echo $result_product[0]['description']; ?></p>
					</div>
				<div class="tuvansp">
					<div class="tuvan">
						<p><b>Tư vấn: </b><ins style="font-size: 20px;"><a style="color: #f72b3f" href="tel:0377301294"><i>0377301294</i></a></ins></p>
					</div>
					<div class="thanhtoans">
						<img src="image/payment.png" alt="" width="190px">	
					</div>	
				</div>
			</div>	
		</div>
		</form>
	</div>
</div>
<!-- hết chi tiết -->

<div class="sale wow flipInY">
	<div class="container">
		<div class="tittle">
			<h2>
				<a class="thanhngang" disabled>Tưng bừng khuyến mãi</a>
			</h2>
		</div>
		<div class="anhsale">
			<img src="image/sale.jpg" alt="" style="width: 100%">
		</div>
	</div>
</div>

<div class="sanphamlq">
	<div class="container">
		<div class="khungbaoquanh">
			<div class="tittle">
				<h2>
					<a class="thanhngang" href="#" style="color: white">Sản phẩm liên quan</a>
				</h2>
			</div>

			<div class="row" style="">
				<?php foreach ($result_category_id as $key => $value): ?>		
					<div class="col-md-4 splq m-0 wow zoomInDown" style="height: 362px;">
					<div class="item" style="background-color: #fff;height: 100%;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
								<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
							</a>
							<div class="caption text-center" style="padding: 10px 0px;">
								<div style="font-size: 15px; height: 100px;">
									<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
									<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
									</a>
									<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
									<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand'] ?></p>
									</a>
									<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
								</div>
								<div>
								<a style="padding: 10px 60.3px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
								</div>
							</div>
							</div>
					</div>	
				<?php endforeach ?>
			</div>
		</div>					
	</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  	<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" role="button" class="close" style="color: #fff;opacity: 1;">
          <span>&times;</span>
	</a>
    <div class="modal-content">
      <div class="modal-body" style="padding: 0px;">
		<div class="container">
		<div class="row">
				<div class="col-md-6" style="padding: 0px;">
					<p style="text-align: center;padding: 10px;border-bottom: 1px solid #dee2e6;border-right: 1px solid #dee2e6;" class="col-md-12"><i class="fas fa-check"></i> Bạn vừa thêm sản phẩm này vào giỏ hàng</p>
					<div class="col-md-12" style="padding: 15px 0px;">
						<div class="row">
							<div class="col-md-12" style="text-align: center;">
								<div class="thumb-1x1">
									<img style="box-shadow: 0px 0px 3px #ccc;" width="150px" height="150px" src="image/<?php echo $image_product ?>" alt="">
								</div>
							</div>
							<div class="col-md-12" style="text-align: center;margin-top: 15px;">
								<div class="product-title"><?php echo $name_product." - ".$size_product." / ".$color_product ?></div>
								<div class="product-new-price" style="color: red;margin-top: 5px;">
									<span><?php echo number_format($price_product,"0",",",".")."đ"?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="padding: 0px;text-align: center;">
					<p style="padding: 10px;border-bottom: 1px solid #dee2e6;" class="col-md-12"><i style="font-size: 18px;" class="fas fa-shopping-cart"></i> Giỏ hàng của bạn có <?php echo $quantity ?> sản phẩm</p>
					<div class="total_price" style="text-align: center;margin: 40px 0px 30px;">
						<div style="color: #555;" class="total_price_h">Tổng tiền</div> 
						<div style="font-size: 24px;" class="price"><?php echo number_format(total_price($cart)+($price_product*$soluong),"0",",",".")."₫" ?></div>
					</div>
					<a href="view-cart.php" class="xemgiohang">
						<i style="font-size: 18px;color: #fff;" class="fas fa-shopping-cart"></i> Tới giỏ hàng
					</a>
				</div>
		</div>
		</div>
      </div>
    </div>
  </div>
</div>
<?php if (isset($_POST['mua'])): ?>
    <script>
        $(document).ready(function(){
            $('#exampleModal').modal('show');
        });
    </script>
<?php endif ?>
<script>
	 function changeImageOnClick(event){
        
        event = event || window.event;
        var targetElement = event.target || event.srcElement;

        if (targetElement.tagName == "IMG")
        {
            document.getElementById("anh-to").src = targetElement.getAttribute("src");
        }
    }
</script>
<?php 
include 'footerfe.php';
?>