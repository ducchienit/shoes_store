<?php
include 'menu.php';
if(isset($_SESSION['admin']) || isset($_SESSION['customer'])){
    return header("location: error.php");
}
if(isset($_GET['code']) && isset($_GET['email'])){
    $code = $_GET['code'];
    $email = $_GET['email'];
    $check = count(execQuery("SELECT * FROM recovery_password WHERE email = '$email' AND code = '$code'"));
    if($check != 0){
        if(isset($_POST['recovery'])){
            if($_POST['new_password'] == $_POST['confirm_password']){
                $new_password = md5($_POST['new_password']);
                execQuery("UPDATE accounts SET password = '$new_password' WHERE email = '$email'");
                execQuery("DELETE FROM recovery_password WHERE email = '$email'");
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã thay đổi mật khẩu',
                })";
                return header("location: login.php");
            }else{
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Lỗi xác nhận',
                    text: 'Mật khẩu không khớp',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
        }
    }else{
        return header("location: login.php");
    }
}else {
    return header("location: login.php");
}
?>
<!-- đăng nhập -->
<div class="banner_noidung">
    <h4>Khôi phục mật khẩu</h4>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 taikhoandn">
            <div class="dangnhap">
                <form action="" method="POST" style="padding: 10px 0px;">
                    <p>Mật khẩu mới</p>
                    <input type="password" name="new_password" placeholder="Nhập mật khẩu mới" required>
                    <p>Xác nhận mật khẩu</p>
                    <input type="password" name="confirm_password" placeholder="Nhập lại mật khẩu mới" required>
                    <input title="Đặt lại mật khẩu" type="submit" name="recovery" value="Đặt lại mật khẩu">
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
include 'footerfe.php';
?>