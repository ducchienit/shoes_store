<?php
include 'menu.php';
if(! isset($_SESSION['customer']) && ! isset($_SESSION['admin'])){
	header("location: login.php");
}
if(empty($_SESSION['cart'])){
	header("location: home.php");
}
if(isset($_SESSION['customer']) || isset($_SESSION['admin'])){
    $full_name = (isset($_SESSION['customer'])) ? $_SESSION['customer'] : $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$full_name'")[0];
}
if(isset($_POST['submit'])){
	$account_id = $_POST['submit'];
	if($_POST['address'] == null){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'Bạn chưa nhập địa chỉ',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
	$address = $_POST['address'];
	$totaol = total_price($cart);
	$inset = "INSERT INTO orders(account_id,total_price,address)VALUES('$account_id','$totaol','$address')";
	$insert_order = execQuery($inset);
	foreach ($cart as $key => $value) {
		$pro_size_id = $value['pro_size_id'];
		$pro_color_id = $value['pro_color_id'];
		$price = $value['price'];
		$quantity = $value['quantity'];
		$insert_oder_detail = execQuery("INSERT INTO order_detail(order_id,product_size_id,product_color_id,price,quantity) VALUES('$insert_order','$pro_size_id','$pro_color_id','$price','$quantity')"); 
	}
	unset($_SESSION['cart']);
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Đặt hàng thành công',
	})";
	return header("location: lich_su_mua.php");
}
?>
<div class="banner_noidung">
	<h4>Thanh toán
	</h4>
	<div class="lien_ket text-center">
	</div>
</div>

<div class="container" style="padding: 40px 0px">
	<form action="" method="POST" class="row">

		<div class="col-md-5" style="margin-top: 40px">
			<h4>Thông tin mua hàng</h4>
			<div>
				<div class="form-group">
					<label for="">Tên người nhận</label>
					<input type="text" class="form-control" value="<?php  echo $info['full_name']; ?>"  placeholder="Tên người nhận" name="name" disabled="">
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<input type="text" class="form-control" value="<?php  echo $info['email']; ?>"  placeholder="Tên người nhận" name="name" disabled="">
				</div>
				<div class="form-group">
					<label for="">Số điện thoại</label>
					<input type="text" class="form-control" value="<?php  echo $info['phone']; ?>"  placeholder="Số điện thoại" name="phone" disabled="">
				</div>
				<div class="form-group">
					<label for="">Địa chỉ người nhận</label>
					<textarea class="form-control" id="address" rows="3" placeholder="Địa chỉ người nhận" name="address"><?php  echo $info['address']; ?></textarea>
				</div>
		</div>	
		</div>
		<div class="col-md-7" style="margin-top: 40px;">
			<div class="tittle_sp">
				<h4 style="color: black;">Đơn hàng (<?php echo $quantity; ?>) sản phẩm</h4>
			</div>
			<div style="border-collapse: collapse;
				border-spacing: 0;
				width: 100%;
				border: 1px solid #ddd;" class="scrollable-nav">
				<table class="table table-hover" style="margin: 0px;">
				<thead>
				<tr style="text-align: center">
					<th scope="col">Ảnh</th>
					<th scope="col">Sản phẩm</th>
					<th scope="col">Loại</th>
					<th scope="col">Số lượng</th>
					<th scope="col">Giá</th>
				</tr>
				</thead>
				<tbody>	
					<?php foreach ($cart as $key => $value) { ?>
						<tr class="product" style="">
							<td class="product__image" style="  text-align: left;
							padding: 8px;">
							<div class="product-thumbnail">
								<div class="product-thumbnail__wrapper" data-tg-static="">	
									<img style="height: 60px;width: 60px;" src="image/<?php echo $value['image'] ?>" alt="" class="product-thumbnail__image" width="100px">
								</div>
								<span class="product-thumbnail__quantity"></span>
							</div>
						</td>
						<td style="text-align: left;"><?php echo $value['name'] ?></td>
						<td style="text-align: left;"><?php echo "Size ".$value['size']." / ".$value['color'] ?></td>
						<td style="  text-align: left;text-align: center"><?php echo $value['quantity'] ?></td>
						<td class="product__price" style="text-align: center;padding-right: 25px;">
							<?php echo number_format($value['price']*$value['quantity'],"0",",",".")."₫"; ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
			<hr style="width: 100%;">
			<div class="total row">
				<div class="total_ctx col-md-12">
					<p style="float: left;font-size: 18px">Tổng tiền thanh toán</p>
					<p style="float: right;font-size: 25px;"><strong><?php echo number_format(total_price($cart),"0",",",".")."₫" ?></strong></p>
				</div>
				<div class="col-md-6" style="display: flex;">
					<a class="xemlaigiohang" href="view-cart.php" style="text-decoration: none;padding: 10px 0px;"><i class="fas fa-chevron-left"></i> <i>Quay lại giỏ hàng</i> </a>
				</div>
				<div class="col-md-6" style="text-align: right;">
					<button style="border: none;" value="<?php  echo $info['account_id']; ?>" type="submit" class="mua" name="submit">Đặt hàng</button>
				</div>
			</div>

		</div>
	</form>
</div>
<?php  
include 'footerfe.php';
?>