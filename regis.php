<?php
include 'menu.php';
if(isset($_SESSION['admin'])){
    return header("location: admin/home");
}
if(isset($_SESSION['customer'])){
    return header("location: home.php");
}
$er=[];
if(isset($_POST['email'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $rpassword = $_POST['rpassword'];
    $phone = $_POST['phone'];
    if(empty($name)){
        $er['name'] = "Bạn chưa nhập họ và tên";
    }
    if(empty($email)){
        $er['email'] = "Bạn chưa nhập email";
    }
    if(empty($password)){
        $er['password'] = "Bạn chưa nhập password";
    }
    if(empty($phone)){
        $er['phone'] = "Bạn chưa nhập số điện thoại";
    }
    if($rpassword != $password){
        $er['rpassword'] = "Mật khẩu không khớp";
    }
    if(empty($er)){
        $check = count(execQuery("SELECT * FROM accounts WHERE email = '$email'"));
        if($check !=0 ){
            $_SESSION['title'] = "Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Email này đã tồn tại',
            })";
            return header('location: regis.php');
        }
        $pass = md5($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO accounts(email,password)VALUES('$email','$pass')";
        $result = execQuery($sql);
        execQuery("INSERT INTO customers (account_id,full_name,phone)VALUES('$result','$name','$phone')");
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Đăng ký thành công',
        })";
        return header('location: login.php');
    }
}
?>

<!-- đăng nhập -->
<div class="banner_noidung">
    <h4>Đăng ký tài khoản</h4>
</div>
<div class="container">
    
        <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 taikhoan">
            <div class="dangnhap">
                <h1 style="font-size: 30px;text-align: center;">Đăng ký tài khoản</h1>
                <form action="" method="POST">
                    <p>Họ và tên</p>
                    <input type="text" name="name" placeholder="Nhập họ và tên">
                    <div class="has-error">
                        <span style="color: red"><?php echo (isset($er['name']))?$er['name']:''?></span>
                    </div> 
                    <p>Email</p>
                    <input type="email" name="email" placeholder="Nhập email" >
                    <div class="has-error">
                        <span style="color: red"><?php echo (isset($er['email']))?$er['email']:''?></span>
                    </div>
                    <p>Số điện thoại</p>
                    <input type="text" name="phone" placeholder="Nhập số điện thoại" >
                    <div class="has-error">
                        <span style="color: red"><?php echo (isset($er['phone']))?$er['phone']:''?></span>
                    </div> 
                    <p>Mật khẩu</p>
                    <input type="password" name="password" placeholder="Nhập mật khẩu" >
                    <div class="has-error">
                        <span style="color: red"><?php echo (isset($er['password']))?$er['password']:''?></span>
                    </div>
                    <p>Nhập lại mật khẩu</p>
                    <input type="password" name="rpassword" placeholder="Nhập mật khẩu" >
                    <div class="has-error">
                        <span style="color: red"><?php echo (isset($er['rpassword']))?$er['rpassword']:''?></span>
                    </div>
                    <input title="Đăng ký" type="submit" name="login" value="Đăng ký">
                </form>
            </div>
        </div>
        </div>
    
</div>
<?php 
include 'footerfe.php';
?>