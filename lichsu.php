<?php  
include 'menu.php';
if(isset($_SESSION['customer'])){
	$use = $_SESSION['customer'];
	$account = execQuery("SELECT * FROM accounts WHERE acc_name = '$use'")[0];
	$account_id = $account['id'];
	$sql_order =execQuery("SELECT * FROM orders WHERE account_id = '$account_id'");
	$history_cart = execQuery("SELECT order_detail.*,products.name,products.image, sizes.size,colors.name as color,product_sizes.id as pro_size_id FROM order_detail 
		INNER JOIN product_sizes ON order_detail.product_size_id = product_sizes.id 
		INNER JOIN sizes ON product_sizes.size_id = sizes.id 
		INNER JOIN product_colors ON order_detail.product_color_id = product_colors.id 
		INNER JOIN colors ON product_colors.color_id = colors.id 
		INNER JOIN products ON products.id = order_detail.product_id WHERE order_detail.product_id = products.id");
	// echo "<pre>";
	// print_r($history_cart);
	// die();
}
if(isset($_GET['id'])){
	$id = $_GET['id'];
	$delete = "DELETE FROM order_detail WHERE id = '$id'";
	$delete_order = execQuery($delete);
	return header('location: lichsu.php');
}
?>
<div class="banner_noidung">
	<h4>Lịch sử đơn hàng</h4>
</div>

<div class="container">
	<div class="row">
		<?php if (count($history_cart) == 0){ ?>
			<?php echo "<span style='font-size: 20px;height: 300px;margin-top:20px'>Không tìm thấy lịch sử mua hàng nào! Vui lòng quay lại <a href='home.php' style='color:red;font-size: 20px;text-decoration: none'>cửa hàng</a> để mua sản phẩm</span>" ?>
		<?php }else{ ?>
		<div class="col-md-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Ảnh sản phẩm</th>
						<th scope="col">Tên sản phẩm</th>
						<th scope="col">Cỡ sản phẩm</th>
						<th scope="col">Màu sản phẩm</th>
						<th scope="col">Số lượng</th>
						<th scope="col">Giá sản phẩm</th>
						<th scope="col">Tác vụ</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($history_cart as $key => $value) { ?>
						<tr>
							<td><img src="image/<?php echo $value['image']?>" alt="" width="100px" height="100px;"></td>
							<td><?php echo $value['name']; ?></td>
							<td><?php echo $value['size']; ?></td>
							<td><?php echo $value['color']; ?></td>
							<td><?php echo $value['quantity']; ?></td>
							<td><?php echo number_format($value['price'] * $value['quantity'],"0",",",".")."₫" ?></td>
							<td><a href="lichsu.php?id=<?php echo $value['id']?>" class="btn btn-danger">Xóa</a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			  <div class="tieptucmua row">

        <div class="col-lg-4 col-md-3"></div>
        <div class="col-lg-8 col-md-9" style="text-align: end;">
        <div class="table-total">
              <table class="table">
                <tbody><tr>
                  <td style="border: none;" class="total-text f-left">Tổng tiền</td>
                  <td style="border: none;" class="txt-right totals_price price_end f-right"><?php echo number_format(total_price($history_cart),"0",",",".")."₫" ?></td>
                </tr>
              </tbody></table>
            </div>
           <a href="view-cart.php" class="btn btn-primary"role="button" style="margin-bottom: 15px;">Quay về giỏ hàng</a>
        </div> 
  		</div>
		</div>
		<?php } ?>
	</div> 
</div>
<?php  
include 'footerfe.php';
?>