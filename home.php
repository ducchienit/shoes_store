<?php  
include 'menu.php';
$product = "SELECT products.*,brands.name as brand FROM products 
	JOIN brands ON products.brand_id = brands.id WHERE status = 1 ORDER BY products.created_at DESC LIMIT 8";
$products = execQuery($product);

$sp_cho_nu = execQuery("SELECT products.*, brands.name as brand FROM products
	INNER JOIN brands ON products.brand_id = brands.id
	INNER JOIN product_styles ON products.id = product_styles.product_id 
	INNER JOIN styles ON product_styles.style_id = styles.id 
	WHERE styles.id = 6 AND products.status = 1 ORDER BY RAND() LIMIT 3");

$sp_cho_name = execQuery("SELECT products.*, brands.name as brand FROM products
	INNER JOIN brands ON products.brand_id = brands.id
	INNER JOIN product_styles ON products.id = product_styles.product_id 
	INNER JOIN styles ON product_styles.style_id = styles.id 
	WHERE styles.id = 5 AND products.status = 1 ORDER BY RAND() LIMIT 3");

$sp_cho_tre_em = execQuery("SELECT products.*, brands.name as brand FROM products
	INNER JOIN brands ON products.brand_id = brands.id
	INNER JOIN product_styles ON products.id = product_styles.product_id 
	INNER JOIN styles ON product_styles.style_id = styles.id 
	WHERE styles.id = 7 AND products.status = 1 ORDER BY RAND() LIMIT 3");

if(isset($_GET['xemsp'])){
	$id = $_GET['xemsp'];
	$product_sp = execQuery("SELECT * FROM products WHERE id = '$id'");
	$select_color = "SELECT products.*,colors.name as color,product_colors.id as pro_color_id FROM products 
	INNER JOIN product_colors ON products.id = product_colors.product_id
	INNER JOIN colors ON product_colors.color_id = colors.id WHERE products.id = $id GROUP BY colors.name";
	$result_color = execQuery($select_color);
	$select_size = "SELECT products.*,brands.name as brand,sizes.size,product_sizes.id as pro_size_id  FROM products 
	INNER JOIN brands ON products.brand_id = brands.id 
	INNER JOIN product_sizes ON products.id = product_sizes.product_id
	INNER JOIN sizes ON product_sizes.size_id = sizes.id WHERE products.id = $id GROUP BY sizes.size";
	$result_size = execQuery($select_size);
}
?>
<div class="banner">
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="carousel-item active">
		<img src="image/banner.jpg" class="d-block w-100" alt="..." style="height: 400px;">
		</div>
		<div class="carousel-item">
		<img src="image/banner-1.jpg" class="d-block w-100" alt="..." style="height: 400px;">
		</div>
		<div class="carousel-item">
		<img src="image/banner-2.png" class="d-block w-100" alt="..." style="height: 400px;">
		</div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="previous" aria-hidden="true"><i class="fas fa-chevron-left"></i></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="next" aria-hidden="true"><i class="fas fa-chevron-right"></i></span>
		<span class="sr-only">Next</span>
	</a>
	</div>
</div>
<!-- hết banner -->
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 25px 15px;">
				<div class="tittle">
					<h2>
						<a class="thanhngang" title="Sản phẩm mới" disabled>Sản phẩm mới</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- spm -->
<div class="sanphammoi" style="margin-bottom: 25px">
	<div class="container">
	<div class="wow bounceInLeft" style="padding: 0px;">
		<div class="row owl-carousel owl-theme" style="margin: 0px;">
			<?php foreach ($products as $key => $value){?>
				<div class="item">
					<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
						<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
					</a>
					<div class="caption text-center" style="padding: 10px 0px;">
						<div style="font-size: 15px; height: 100px;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
							<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
							</a>
							<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
							<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand']; ?></p>
							</a>
							<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
						</div>
						<div>
						<a style="padding: 10px 55.3px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>	
	</div>
	</div>
</div>
<!-- hết sp -->

<!-- hãng giày -->
<div class="brand d-block d-sm-none" >
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="tittle">
					<h2>
						<a class="thanhngang" href="#">BRAND</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="thuonghieu wow fadeInLeft" style="margin-bottom: 25px;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">	
				<div class="banner1">
					<img src="image/cap-da-nam_1.jpg" alt="" width="100%" class="zoom">
				</div>
			</div>

			<div class="col-md-6">	
				<div class="banner2">
					<img src="image/cap-da-nam_1.jpg" alt="" width="100%" class="zoom">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- sẩn phẩm -->
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="tittle">
					<h2>
						<a class="thanhngang" title="Sản phẩm nổi bật" disabled>Sản phẩm nổi bật</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sanpham">
	<div class="container">
		<div class="row">
			<div class="" style="margin-top: 30px;width: 100%;margin-left: 0px;margin-right: 0px;display: flex;">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding-right: 0px;">
					<div class="banner_img">
						<img class="bg_module" src="image/bg_module_1.jpg" alt="">
						<h2 class="title-base" title="Cho nữ">
							<a href="timsp_pc.php?id=6" class="headline">Cho nữ</a>
						</h2>
						<span>Cung cấp những sản phẩm bộ sưu tập mới nhất cho bạn</span>
						<a href="timsp_pc.php?id=6" class="btn-shop-now" title="Xem thêm">Xem thêm</a>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 row" style="padding-right: 0px;margin: 0px;">
					<?php foreach($sp_cho_nu as $key => $value) {?>
						<div class="col-md-4" style="height: 363px;">
							<div class="item" style="box-shadow: 0px 0px 5px #ccc;height: 100%;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
								<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
							</a>
							<div class="caption text-center" style="padding: 10px 0px;">
								<div style="font-size: 15px; height: 100px;">
									<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
									<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
									</a>
									<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
									<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand'] ?></p>
									</a>
									<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
								</div>
								<div>
								<a style="padding: 10px 77.3px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
								</div>
							</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="sanpham">
	<div class="container">
		<div class="row">
			<div class="" style="margin-top: 30px;width: 100%;margin-left: 0px;margin-right: 0px;display: flex;">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding-right: 0px;">
					<div class="banner_img">
						<img class="bg_module" src="image/bg_module_2.jpg" alt="">
						<h2 class="title-base" title="Cho nam">
							<a href="timsp_pc.php?id=6" class="headline">Cho nam</a>
						</h2>
						<span>Cung cấp những sản phẩm bộ sưu tập mới nhất cho bạn</span>
						<a href="timsp_pc.php?id=5" class="btn-shop-now" title="Xem thêm">Xem thêm</a>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 row" style="padding-right: 0px;margin: 0px;">
					<?php foreach($sp_cho_name as $key => $value) {?>
						<div class="col-md-4" style="height: 363px;">
							<div class="item" style="box-shadow: 0px 0px 5px #ccc;height: 100%;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
								<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
							</a>
							<div class="caption text-center" style="padding: 10px 0px;">
								<div style="font-size: 15px; height: 100px;">
									<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
									<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
									</a>
									<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
									<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand'] ?></p>
									</a>
									<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
								</div>
								<div>
								<a style="padding: 10px 77.3px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
								</div>
							</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="sanpham">
	<div class="container">
		<div class="row">
			<div class="" style="margin-top: 30px;width: 100%;margin-left: 0px;margin-right: 0px;display: flex;">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding-right: 0px;">
					<div class="banner_img">
						<img class="bg_module" src="image/bg_module_3.jpg" alt="">
						<h2 class="title-base" title="Cho trẻ em">
							<a href="timsp_pc.php?id=7" class="headline">Cho trẻ em</a>
						</h2>
						<span>Cung cấp những sản phẩm bộ sưu tập mới nhất cho bạn</span>
						<a href="timsp_pc.php?id=7" class="btn-shop-now" title="Xem thêm">Xem thêm</a>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 row" style="padding-right: 0px;margin: 0px;">
					<?php foreach($sp_cho_tre_em as $key => $value) {?>
						<div class="col-md-4" style="height: 363px;">
							<div class="item" style="box-shadow: 0px 0px 5px #ccc;height: 100%;">
							<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>">
								<img src="image/<?php echo $value['image']; ?>" alt="" width="100%" style="height: 220px">
							</a>
							<div class="caption text-center" style="padding: 10px 0px;">
								<div style="font-size: 15px; height: 100px;">
									<a title="<?php echo $value['name']; ?>" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" style="color: #252525;">
									<p style="height: 30%;margin: 0px;"><?php if(strlen($value['name'])>28)  echo (substr($value['name'], 0, 28)."...") ?><?php else echo $value['name'] ?></p>
									</a>
									<a style="text-decoration: none;" title="<?php echo $value['brand'] ?>" href="timsp_th.php?id=<?php echo $value['brand_id'] ?>">
									<p style="margin-bottom: 5px;color: darkgray;"><?php echo $value['brand'] ?></p>
									</a>
									<p style="color:  #f72b3f;"><?php echo number_format($value['price'],"0",",",".")."₫"; ?></p>
								</div>
								<div>
								<a style="padding: 10px 77.3px;" title="Xem chi tiết" href="chi-tiet-san-pham.php?sp=<?php echo $value['id'] ?>" class="mua">Xem chi tiết</a>
								</div>
							</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- <div class="phantrang" style="margin-top: 10px">
	<nav aria-label="Page navigation example">
		<ul class="pagination" style="margin-left: 50%">
			<?php if($cr_page -1 > 0) { ?>
			<li class="page-item">
				<a class="page-link" href="home.php?page=<?php echo $cr_page - 1 ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>
			<?php } ?>
			<?php for ($i=1; $i<=$page; $i++ ){ ?>
			<li class="<?php echo (($cr_page == $i)? 'page-item active' : '') ?>">
				<a class="page-link" href="home.php?page=<?php echo $i?>"><?php echo $i ?></a></li>
			<?php } ?>
			<?php if($cr_page +1 <=  $page) { ?>
			<li class="page-item">
				<a class="page-link" href="home.php?page=<?php echo $cr_page + 1 ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<?php } ?>
		</ul>
	</nav>
</div> -->

</div>
</div>
<!-- hết sp -->

<!-- liên hệ -->

<section class="awe-section-8">	
	<div class="section_hotline lazyload" data-src="//bizweb.dktcdn.net/100/369/492/themes/741260/assets/bg_hotline.jpg?1594607980046" data-was-processed="true" style="background-image: url(&quot;//bizweb.dktcdn.net/100/369/492/themes/741260/assets/bg_hotline.jpg?1594607980046&quot;);">
		<div class="parallax">
			<div class="wrapper">   
				<div class="custom-center text-center">
					<div class="custom-center-body">
						<h2>Đường dây nóng</h2>			
						<a class="hai01" href="tel:0123456789" style="text-decoration: none">0123456789</a>		
						<p>Chúng tôi cam kết 100% các sản phẩm giày dép của chúng tôi nguồn gốc xuất xứ nhập khẩu chính hãng 100%</p>
					</div>  
				</div>
			</div>    
		</div>
	</div>
</section>
<!-- het loen he -->

<!-- anh shop -->
<!-- <div class="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="tittle">
					<h2>
						<a class="thanhngang" href="#">Tin thời trang</a>
					</h2>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="tinthoitrang">
	<div class="container">
		<div class="row">
			<div class="col-md-4 wow slideInLeft">
				<div class="img_new">
					<a href="#"><img src="https://bizweb.dktcdn.net/thumb/large/100/369/492/articles/photo-1514989940723-e8e51635b782.jpg?v=1572232563380" alt="" width="100%"></a>
				</div>
				<div class="time_content">
					<i class="far fa-calendar far"></i>
					<b class="time_dang">10/08/2020</b> &nbsp;&nbsp; Đăng bởi: <b class="content_name">Tân</b>
				</div>
				<div class="content_new">
					<div class="content_nd">							
						<h3>
							<a href="#" title="Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ" style="text-decoration: none">Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ</a>
						</h3>
					</div>

					<div class="content_nd2">
						<p> Adidas là một trong những hàng thời trang sneaker lớn nhất hiện nay của thế giới ...</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 wow slideInLeft">
				<div class="img_new">
					<a href="#"><img src="https://bizweb.dktcdn.net/thumb/large/100/369/492/articles/photo-1514989940723-e8e51635b782.jpg?v=1572232563380" alt="" width="100%"></a>
				</div>
				<div class="time_content">
					<i class="far fa-calendar far"></i>
					<b class="time_dang">10/08/2020</b> &nbsp;&nbsp; Đăng bởi: <b class="content_name">Tân</b>
				</div>
				<div class="content_new">
					<div class="content_nd">							
						<h3>
							<a href="#" title="Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ" style="text-decoration: none">Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ</a>
						</h3>
					</div>

					<div class="content_nd2">
						<p> Adidas là một trong những hàng thời trang sneaker lớn nhất hiện nay của thế giới ...</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 wow slideInLeft">
				<div class="img_new">
					<a href="#"><img src="https://bizweb.dktcdn.net/thumb/large/100/369/492/articles/photo-1514989940723-e8e51635b782.jpg?v=1572232563380" alt="" width="100%"></a>
				</div>
				<div class="time_content">
					<i class="far fa-calendar far"></i>
					<b class="time_dang">10/08/2020</b> &nbsp;&nbsp; Đăng bởi: <b class="content_name">Tân</b>
				</div>
				<div class="content_new">
					<div class="content_nd">							
						<h3>
							<a href="#" title="Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ" style="text-decoration: none">Hướng dẫn cách làm trắng đế giày bị ố vàng chuẩn chỉ</a>
						</h3>
					</div>

					<div class="content_nd2">
						<p> Adidas là một trong những hàng thời trang sneaker lớn nhất hiện nay của thế giới ...</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form method="GET" action="cart.php">
        <div class="row"> 

        	<div class="col-md-6">
        		<img src="image/<?php echo $product_sp[0]['image']?>" alt="" width="200px">
        	</div>
        	<div class="col-md-6 noidung_chitiet wow slideInRight">
							<div>
								<h5>Tên sản phẩm: <?php echo $result_color[0]['name']; ?></h5>
							</div>
							<span style="color:#f72b3f">Cỡ giày:</span>
							<div class="checkbox">
								<?php foreach ($result_size as $key => $value): ?>	
									<label>
										<input type="radio" value="<?php echo $value['pro_size_id']; ?>" name="size" required="">
										<?php echo $value['size']; ?>
									</label>
								<?php endforeach ?>
							</div>

							<span style="color:#f72b3f">màu giày:</span>
							<div class="checkbox">
								<?php foreach ($result_color as $key => $value): ?>	
									<label>
										<input type="radio" value="<?php echo $value['pro_color_id']; ?>" name="color" required="">
										<?php echo $value['color']; ?>
									</label>
								<?php endforeach ?>
							</div>
							<div class="giasanpham">
								<span>Giá sản phẩm: <span class="giasp"><?php echo number_format($products[0]['price']); ?></span></span>
							</div>
							<div class="soluongsp">				
								<p style="margin:0px">Số lượng: </p>
								<input type="number" name="quantity" value="1"> 
								<input type="hidden" name="id" value="<?php echo $result_color[0]['id']; ?>"> 
								<button type="submit" class="btn btn-info">Mua</button>			
							</div> 	
						</div> 	
        </div>
        <form>
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>
<?php if(isset($_GET['xemsp'])) { ?>
	<script>
		$(document).ready(function(){
			$('#staticBackdrop').modal('show');
		})
	</script>";
	<?php } ?>
<!-- het anh shop -->
<!-- fotter -->
<?php  
include 'footerfe.php';
?>