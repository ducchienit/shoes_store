<?php
    function connect(){
        $connect = mysqli_connect("localhost","root","","shoes_shop") or die("connect failed");
        mysqli_set_charset($connect,"utf8");
        return $connect;
    }
    function execQuery($query){
		$conn = connect();
		$type = substr($query, 0, 6);
		if ($type == 'SELECT') {
			$result = mysqli_query($conn, $query);
			return mysqli_fetch_all($result, MYSQLI_ASSOC);
		}
		else {
			$result = mysqli_query($conn, $query);
			return mysqli_insert_id($conn);
		}
	}
?>