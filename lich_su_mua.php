<?php
include "menu.php";
if(! isset($_SESSION['customer']) && ! isset($_SESSION['admin'])){
	header("location: login.php");
}
if(isset($_SESSION['customer']) || isset($_SESSION['admin'])){
    $full_name = (isset($_SESSION['customer'])) ? $_SESSION['customer'] : $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$full_name'")[0];
    $account_id = $info['account_id'];
    $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id'");
    $all = count(execQuery("SELECT * FROM orders WHERE account_id = '$account_id'"));
    $pending = count(execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 0"));
    $shipping = count(execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 1"));
    $delivered = count(execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 2"));
    $cancelled = count(execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 3"));
    $total = count($order);
    $limit = 5;
    $page = ceil($total/$limit);
    $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
    $start = ($page_cr - 1)*$limit;
    $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' ORDER BY created_at DESC LIMIT $start,$limit");
}
if(isset($_POST['view'])){
    $id = $_POST['view'];
    $order_detail = execQuery("SELECT * FROM orders WHERE id = '$id'")[0];
    $sql_order_detail= execQuery("SELECT order_detail.*, 
        sizes.size as size, 
        colors.name as color, 
        products.name as name_product,
        products.image as image_product,
        orders.total_price as total_price,
        orders.status as status
        FROM order_detail 
        INNER JOIN product_sizes ON order_detail.product_size_id = product_sizes.id 
        INNER JOIN sizes ON product_sizes.size_id = sizes.id 
        INNER JOIN product_colors ON order_detail.product_color_id = product_colors.id 
        INNER JOIN colors ON product_colors.color_id = colors.id 
        INNER JOIN products ON product_sizes.product_id = products.id
        INNER JOIN orders ON order_detail.order_id = orders.id 
        WHERE order_detail.order_id = '$id' ORDER BY order_detail.id");
}
if(isset($_GET['status'])){
    switch ($_GET['status']) {
        case "pending":
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 0");
            $total = count($order);
            $limit = 5;
            $page = ceil($total/$limit);
            $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
            $start = ($page_cr - 1)*$limit;
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 0 ORDER BY created_at DESC LIMIT $start,$limit");
            break;
        case "shipping";
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 1");
            $total = count($order);
            $limit = 5;
            $page = ceil($total/$limit);
            $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
            $start = ($page_cr - 1)*$limit;
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 1 ORDER BY created_at DESC LIMIT $start,$limit");
            break;
        case "delivered":
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 2");
            $total = count($order);
            $limit = 5;
            $page = ceil($total/$limit);
            $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
            $start = ($page_cr - 1)*$limit;
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 2 ORDER BY created_at DESC LIMIT $start,$limit");
            break;
        case "cancelled":
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 3");
            $total = count($order);
            $limit = 5;
            $page = ceil($total/$limit);
            $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
            $start = ($page_cr - 1)*$limit;
            $order = execQuery("SELECT * FROM orders WHERE account_id = '$account_id' AND status = 3 ORDER BY created_at DESC LIMIT $start,$limit");
            break;
    }
}
if(isset($_POST['huy'])){
    $id = $_POST['huy'];
    execQuery("UPDATE orders SET status = 3 WHERE id = '$id'");
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Đã hủy đơn hàng',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['mualai'])){
    $id = $_POST['mualai'];
    execQuery("UPDATE orders SET status = 0, created_at = CURRENT_TIMESTAMP()  WHERE id = '$id'");
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Đã đặt lại',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
?>
<div class="container">
    <div class="row" style="margin: 40px 0px;">
        <form class="col-md-3">
            <div class="list-group" style="background: #252525;color: #fff">
                <a href="lich_su_mua.php" role="button" class="list-group-item list-group-item-action">Tất cả <?php if($all >= 0) {?><?php echo "(".$all.")" ?><?php }?></a>
                <a href="lich_su_mua.php?status=pending" role="button"  class="list-group-item list-group-item-action">Đang chờ <?php if($pending >= 0) {?><?php echo "(".$pending.")" ?><?php }?></a>
                <a href="lich_su_mua.php?status=shipping"  role="button" class="list-group-item list-group-item-action">Đang giao <?php if($shipping >= 0) {?><?php echo "(".$shipping.")" ?><?php }?></a>
                <a href="lich_su_mua.php?status=delivered" role="button" class="list-group-item list-group-item-action">Đã nhận <?php if($delivered >= 0) {?><?php echo "(".$delivered.")" ?><?php }?></a>
                <a href="lich_su_mua.php?status=cancelled" role="button" class="list-group-item list-group-item-action">Đã hủy <?php if($cancelled >= 0) {?><?php echo "(".$cancelled.")" ?><?php }?></a>
            </div>
        </form>
        <div class="col-md-9">
            <h3 class="lichsumua">Lịch sử mua hàng</h3>
            <table class="table table-bordered table-hover">
            <thead>
                <tr>
                <th scope="col">Mã đơn hàng</th>
                <th scope="col">Thời gian</th>
                <th scope="col">Địa chỉ nhận</th>
                <th scope="col">Tổng tiền</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Tác vụ</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($order as $key=>$value) {?>
                <tr>
                <th scope="row">#DH_0<?php echo $value['id'] ?></th>
                <td><?php echo date("d/m/Y H:i:s", strtotime($value['created_at'])); ?></td>
                <td><?php echo $value['address'] ?></td>
                <td><?php echo number_format($value['total_price'],"0",",",".")."₫" ?></td>
                <td><?php switch ($value['status']) {
                case 1:
                    echo("Đang giao");
                    break;
                case 2:
                    echo("Đã nhận");
                    break;
                case 3:
                    echo("Đã hủy");
                    break;
                default:
                    echo("Đang chờ");
                } ?></td>
                <td>
                <form method="POST">
                <button name="view" value="<?php echo $value['id'] ?>" class="btn btn-sm btn-secondary">Xem chi tiết</button>
                </form>
                </td>
                </tr>
            <?php }?>
            </tbody>
            </table>
            <?php if ($page > 1): ?>
            <div style="float: right;">
                <nav aria-label="...">
                    <ul class="pagination">
                        <?php if ($page_cr>1) {?>
                        <li class="page-item">
                        <a href="lich_su_mua.php?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $page_cr -1 ?>" style="color: #252525;" class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fas fa-chevron-circle-left"></i></a>
                        </li>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $page ; $i++) {?>
                        <li id="page<?php echo $i ?>" class="page-item" aria-current="page">
                            <a href="lich_su_mua.php?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $i ?>" style="color: #252525;" class="page-link" href="#"><?php echo $i ?></a>
                        </li>
                        <?php } ?>
                        <?php if ($page_cr<$page) {?>
                        <li class="page-item">
                        <a href="lich_su_mua.php?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $page_cr +1 ?>"" style="color: #252525;" class="page-link" href="#"><i class="fas fa-chevron-circle-right"></i></a>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
            <?php endif ?>

        </div>
    </div>
</div>
<div class="modal fade" id="view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" role="button" class="close" style="color: #fff;opacity: 1;">
          <span>&times;</span>
	</a>        
    <div class="modal-content">          
      <div class="modal-body">
      <div class="don_hang" style="padding: 10px 0px 20px;">
                <h5>Mã đơn hàng: #DH_0<?php echo $order_detail['id'] ?></h5>
                <p>- Đặt hàng ngày: <strong><?php echo date("d/m/Y H:i:s", strtotime($order_detail['created_at'])); ?></strong></p>
                <p>- Địa chỉ giao hàng: <strong><?php echo $order_detail['address'] ?></strong></p>
                <p>- Tổng thanh toán: <strong><?php echo number_format($order_detail['total_price'],"0",",",".")."₫" ?></strong></p>
                <p>- Trạng thái: <strong><?php switch ($order_detail['status']) {
                case 1:
                    echo("Đang giao");
                    break;
                case 2:
                    echo("Đã nhận");
                    break;
                case 3:
                    echo("Đã hủy");
                    break;
                default:
                    echo("Đang chờ");
                } ?></strong></p>
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">STT</th>
                        <th scope="col">HÌNH ẢNH</th>
                        <th scope="col">TÊN SẢN PHẨM</th>
                        <th scope="col">LOẠI HÀNG</th>
                        <th scope="col">SỐ LƯỢNG</th>
                        <th scope="col">GIÁ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($sql_order_detail as $key=>$value) {?>
                        <tr>
                        <th class="text-center" scope="row">1</th>
                        <td class="text-center"><img style="height: 60px;width: 60px;" src="image/<?php echo $value['image_product'] ?>" alt=""></td>
                        <td><?php echo $value['name_product'] ?></td>
                        <td><?php echo "Size ".$value['size']." / ".$value['color'] ?></td>
                        <td class="text-center"><?php echo $value['quantity'] ?></td>
                        <td><?php echo number_format($value['price'],"0",",",".")."₫" ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <form action="" method="POST">
                <?php if($order_detail['status'] == 0) {?>
                <button style="border: none;padding: 10px;" name="huy" value="<?php echo $order_detail['id'] ?>" class="mua">Hủy đơn hàng&nbsp<i class="fas fa-trash-alt"></i></button>
                <?php }?>
                <?php if($order_detail['status'] == 3) {?>
                <button style="border: none;padding: 10px;" name="mualai" value="<?php echo $order_detail['id'] ?>" class="mua">Mua lại&nbsp<i style="color: #fff;font-size: 18px;" class="fas fa-shopping-cart"></i></button>
                <?php }?>
                </form>
                </div>
      </div>
    </div>
  </div>
</div>
<?php if (isset($_POST['view'])): ?>
    <script>
        $(document).ready(function(){
            $('#view').modal('show');
        });
    </script>
<?php endif ?>
<script>
    function $_GET(param) {
        var vars = {};
        window.location.href.replace( location.hash, '' ).replace( 
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;	
        }
        return vars;
    }
    var name = $_GET('page');
    if(name == "null"){
        document.getElementById("page1").classList.add("active-pa");
    }
    document.getElementById("page"+name).classList.add("active-pa");
</script>
<?php
include "footerfe.php";
?>