<?php 
include 'connect.php';
session_start();
if(isset($_GET['id'])){
	$id= $_GET['id'];
}
// kiểm tra xem người dùng mua ở trang chủ hay trang chi tiết
$action = (isset($_GET['action'])) ? $_GET['action'] : 'add';
// kiểm tra xem người dùng có phải mua ở trong chi tiết sp k
// nếu đúng thì thêm số lượng do ng dùng chọn còn không thì mặc định là 1
$quantity = (isset($_GET['quantity'])) ? $_GET['quantity'] : 1;
// var_dump($_GET);
// die();
$size_and_color = $_GET['pro_size_id']+$_GET['pro_color_id'];
$pro_size_id = $_GET['pro_size_id'];
$size = execQuery("SELECT product_sizes.*, sizes.size 
	FROM product_sizes
	INNER JOIN sizes ON product_sizes.size_id = sizes.id
	WHERE product_sizes.id = '$pro_size_id'");
$pro_color_id = $_GET['pro_color_id'];
$color = execQuery("SELECT product_colors.*, colors.name as color
	FROM product_colors
	INNER JOIN colors ON product_colors.color_id = colors.id
	WHERE product_colors.id = '$pro_color_id'");
if($quantity <= 0){
	$quantity =1;
}
$product = "SELECT * FROM products WHERE id = '$id'";
$products = execQuery($product);
// khai báo mảng để lưu trữ dữ liệu
$item = [
	'id'=>$products[0]['id'],
	'name'=>$products[0]['name'],
	'image'=>$products[0]['image'],
	'price'=>$products[0]['price'],
	'pro_color_id'=>$pro_color_id,
	'pro_size_id'=>$pro_size_id,
	'size'=>$size[0]['size'],
	'color'=>$color[0]['color'],
	'quantity'=> $quantity
];
// thêm sp vào giỏ hàng
// khi ấn vào nút thêm đầu tiên mặc định cho nó là add
if($action == 'add'){
	// kiểm tra xem sản phẩm này có hay chưa trong giỏ hàng
	if(isset($_SESSION['cart'][$size_and_color])){
		
		$_SESSION['cart'][$size_and_color]['quantity'] += $quantity ;
		
	}else{
		// chưa có thì thêm vào giỏ hàng
		$_SESSION['cart'][$size_and_color] = $item;
	}
}
// kiểm tra nếu action mà bằng update thì lấy số lượng ms nhập từ input
if($action == 'update'){
	$_SESSION['cart'][$size_and_color]['quantity'] = $quantity;
}
// ktra nếu action = delete thì xóa sp theo id
if($action == 'delete'){
	// echo ($id);
	// die();
	unset($_SESSION['cart'][$size_and_color]);
}
header('location: view-cart.php');


?>