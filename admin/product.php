<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$sqlProduct = execQuery("SELECT * FROM products");
$sql_category = "SELECT * FROM categories";
$sql_brand = "SELECT * FROM brands";
$sql_size = "SELECT * FROM sizes ORDER BY sizes.size ASC";
$sql_color = "SELECT * FROM colors";
$sql_style = "SELECT * FROM styles";
$categories1 = execQuery($sql_category);
$brands1 = execQuery($sql_brand);
$sizes1 = execQuery($sql_size);
$colors1 = execQuery($sql_color);
$styles1 = execQuery($sql_style);
$sql = "SELECT * FROM products";
$row = execQuery($sql);
$total = count($row);
$limit = 8;
$page = ceil($total/$limit);
$page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
$start = ($page_cr - 1)*$limit;
$sql_product = "SELECT products.*, brands.name as brand, categories.name as category 
    FROM products 
    INNER JOIN brands ON products.brand_id = brands.id 
    INNER JOIN categories ON products.category_id = categories.id 
    ORDER BY products.id LIMIT $start,$limit";
$result = execQuery($sql_product);
if(isset($_POST['nhap'])){
    header('location: product?search='.$_POST['nhap']);
}
if(isset($_GET['search'])){
    if($_GET['search'] == ""){
        header('location: product');
    }
    $name_search = str_replace("'","\'",$_GET['search']);
    $sql_product = "SELECT products.*, brands.name as brand, categories.name as category 
        FROM products 
        INNER JOIN brands ON products.brand_id = brands.id 
        INNER JOIN categories ON products.category_id = categories.id 
        WHERE products.name LIKE '%$name_search%' ORDER BY products.id";
    $result = execQuery($sql_product);
    $_SESSION['search'] = $_GET['search'];
}
$categories = execQuery("SELECT * FROM categories");
$brands = execQuery("SELECT * FROM brands");
if(isset($_GET['brand'])){
    $name_brand = $_GET['brand'];
    $brand =  execQuery("SELECT * FROM brands WHERE name = '$name_brand'")[0];
    $brand_id = $brand['id'];
    $sql_product = "SELECT * FROM products WHERE brand_id = '$brand_id'"; 
    $row = execQuery($sql_product);
    $total = count($row);
    $limit = 8;
    $page = ceil($total/$limit);
    $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
    $start = ($page_cr - 1)*$limit;
    $sql_product = "SELECT products.*, brands.name as brand, categories.name as category 
        FROM products 
        INNER JOIN brands ON products.brand_id = brands.id 
        INNER JOIN categories ON products.category_id = categories.id 
        WHERE products.brand_id = '$brand_id' LIMIT $start,$limit";
    $result = execQuery($sql_product);
}
if(isset($_GET['category'])){
    $name_category = $_GET['category'];
    $category =  execQuery("SELECT * FROM categories WHERE name = '$name_category'")[0];
    $cate_id = $category['id'];
    $sql_product = "SELECT * FROM products WHERE category_id = '$cate_id'"; 
    $row = execQuery($sql_product);
    $total = count($row);
    $limit = 8;
    $page = ceil($total/$limit);
    $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
    $start = ($page_cr - 1)*$limit;
    $sql_product = "SELECT products.*, brands.name as brand, categories.name as category 
        FROM products 
        INNER JOIN brands ON products.brand_id = brands.id 
        INNER JOIN categories ON products.category_id = categories.id 
        WHERE products.category_id = '$cate_id' LIMIT $start,$limit";
    $result = execQuery($sql_product);
}
if(isset($_POST['view'])){
    $id = $_POST['view'];
    $product_images = $_POST['view'];
    $products = execQuery("SELECT * FROM products WHERE id = '$id'")[0];
    $images = execQuery("SELECT * FROM product_images WHERE product_id = '$id'");
    $product_size = execQuery("SELECT * FROM product_sizes WHERE product_id = '$id' AND status = '1'");
    $product_color = execQuery("SELECT * FROM product_colors WHERE product_id = '$id' AND status = '1'");
    $product_style = execQuery("SELECT * FROM product_styles WHERE product_id = '$id'");
    $check_size = [];
    $check_style = [];
    $check_color = [];
    foreach($product_size as $key => $value){        
        array_push($check_size,$value['size_id']);
    }
    foreach($product_style as $key => $value){        
        array_push($check_style,$value['style_id']);
    }
    foreach($product_color as $key => $value){        
        array_push($check_color,$value['color_id']);
    }
}

if(isset($_POST['update'])){
    $id = $_POST['update'];
    $check = execQuery("SELECT * FROM products EXCEPT SELECT * FROM products WHERE id = '$id'");
    $not_name = str_replace(" ","",$_POST['name']);
    $name_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['name']);
        array_push($name_array,$name_value);
    }
    if (in_array($not_name,$name_array)) { // Yoshi version
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'This product name already exists',
        })";
        return header("location: ".$_SERVER['REQUEST_URI']);
    }
    $name = str_replace("'","\'",$_POST['name']);
    $data = $_POST['price'];
    $price = str_replace(".","",$data);
    if($price == 0){
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Product price must be greater than 0',
        })";
        return header("location: ".$_SERVER['REQUEST_URI']);
    }
    $category_id = $_POST['category'];
    $brand_id = $_POST['brand'];
    $description = $_POST['description'];
    $status = $_POST['status'];
    $sizes_insert = $_POST['size'];
    $colors_insert = $_POST['color'];
    $styles_insert = $_POST['style'];
    if (isset($_FILES['image'])) {
		$file = $_FILES['image'];
        $file_name = $file['name'];
        $file_type = strtolower(end(explode('.',$file_name)));
        $type =  array("jpeg","jpg","png");
        $image_name = str_replace("'","\'",$file['name']);
        if($file_name != null){
            if(in_array($file_type,$type)=== false){
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Only supports upload JPEG or PNG files.',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            if($file['size'] > 2097152) {
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'The file size cannot be more than 2MB.',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            if ($file['error'] != 0){
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Error upload data',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            if(is_uploaded_file($file['tmp_name']) == FALSE){
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'An error occurred while uploading the file.',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            move_uploaded_file($file['tmp_name'],'../image/'.$file_name);
        }
        if ($file_name != null) {
            execQuery("UPDATE products SET name='$name',price='$price',category_id='$category_id',brand_id='$brand_id',description='$description',image='$image_name',status='$status' where id = '$id' ");
            header('location: product.php');
        }
        else {
            execQuery("UPDATE products SET name='$name',price='$price',category_id='$category_id',brand_id='$brand_id',description='$description',status='$status' where id = '$id' ");
            header('location: product.php');
        }
    }
    if (isset($_FILES['images'])) {
		$files = $_FILES['images'];
		$file_names = $files['name'];
        $type =  array("jpeg","jpg","png");
        if($files['name'][0] != null){
            foreach($file_names as $key => $value){
                $file_types = strtolower(end(explode('.',$value)));
                if(in_array($file_types,$type) == FALSE){
                    $_SESSION['title'] = "Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Only supports upload JPEG or PNG files.',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            foreach($files['error'] as $key => $value){
                if($value != 0){
                    $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Error upload data',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            if(array_sum($files['size']) > 5242880){
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'The file size should not be larger than 5MB.',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            foreach($files['tmp_name'] as $key => $value){
                if(is_uploaded_file($value) == FALSE){
                    $_SESSION['title'] = "Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'An error occurred while uploading the file.',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            foreach($file_names as $key => $value){
                $images_name =  str_replace("'","\'",$value);
                move_uploaded_file($files['tmp_name'][$key],'../image/'.$value);             
            }   
        }
        if(strlen($file_names[0]) > 0){
            execQuery("DELETE FROM product_images WHERE product_id = '$id'");
            foreach($file_names as $key => $value){
                $images_name = str_replace("'","\'",$value);
                execQuery("INSERT INTO product_images (product_id,image) VALUES ('$id','$images_name')");
            }
        }
    }
    if($sizes_insert != null){
        $product_size = execQuery("SELECT * FROM product_sizes WHERE product_id = '$id'");
        $product_size_array = [];
        foreach($product_size as $key => $value){
            array_push($product_size_array,$value['size_id']);
        }
        execQuery("UPDATE product_sizes SET status='0' where product_id = '$id'");
        foreach($sizes_insert as $key => $value){
            if(in_array($value,$product_size_array)){
                execQuery("UPDATE product_sizes SET status='1' where product_id = '$id' and size_id = '$value'");
            }else{
                execQuery("INSERT INTO product_sizes (size_id,product_id) VALUES ('$value','$id')");
            }
        }
    }
    if($colors_insert != null){
        $product_color = execQuery("SELECT * FROM product_colors WHERE product_id = '$id'");
        $product_color_array = [];
        foreach($product_color as $key => $value){
            array_push($product_color_array,$value['color_id']);
        }
        execQuery("UPDATE product_colors SET status='0' where product_id = '$id'");
        foreach($colors_insert as $key => $value){
            if(in_array($value,$product_color_array)){
                execQuery("UPDATE product_colors SET status='1' where product_id = '$id' and color_id = '$value'");
            }else{
                execQuery("INSERT INTO product_colors (color_id,product_id) VALUES ('$value','$id')");
            }
        }
    }
    if($styles_insert != null){
        execQuery("DELETE FROM product_styles WHERE product_id = '$id'");
        foreach($styles_insert as $key => $value){
            execQuery("INSERT INTO product_styles (style_id,product_id) VALUES ('$value','$id')");
        }
    }
    $_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Update Product Success',
        showConfirmButton: false,
        timer: 1500
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['add'])){
    $check = execQuery("SELECT * FROM products");
    $not_name = str_replace(" ","",$_POST['name']);
    $name_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['name']);
        array_push($name_array,$name_value);
    }
    if (in_array($not_name,$name_array)) { // Yoshi version
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'This product name already exists',
        })";
        return header("location: ".$_SERVER['REQUEST_URI']);
    }
    $name = str_replace("'","\'",$_POST['name']);
    $price = str_replace(".","",$_POST['price']);
    if($price == 0){
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Product price must be greater than 0',
        })";
        return header("location: ".$_SERVER['REQUEST_URI']);
    }
    $category_id = $_POST['category'];
    $brand_id = $_POST['brand'];
    $description = $_POST['description'];
    $status = $_POST['status'];
    $sizes_insert = $_POST['size'];
    $colors_insert = $_POST['color'];
    $styles_insert = $_POST['style'];
    if (isset($_FILES['image'])) {
        $file = $_FILES['image'];
        $file_name = $file['name'];
        $file_type = strtolower(end(explode('.',$file_name)));
        $type =  array("jpeg","jpg","png");
        $image_name = str_replace("'","\'",$file['name']);
        if(in_array($file_type,$type)=== false){
            $_SESSION['title'] = "Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Only supports upload JPEG or PNG files.',
            })";
            return header("location: ".$_SERVER['REQUEST_URI']);
        }
        if($file['size'] > 2097152) {
            $_SESSION['title'] = "Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'The file size cannot be more than 2MB.',
            })";
            return header("location: ".$_SERVER['REQUEST_URI']);
        }
        if ($file['error'] != 0){
            $_SESSION['title'] = "Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Error upload data',
            })";
            return header("location: ".$_SERVER['REQUEST_URI']);
        }

        if(is_uploaded_file($file['tmp_name']) == FALSE){
            $_SESSION['title'] = "Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'An error occurred while uploading the file.',
            })";
            return header("location: ".$_SERVER['REQUEST_URI']);
        }
        move_uploaded_file($file['tmp_name'],'../image/'.$file_name);
    }
    if (isset($_FILES['images'])) {
        $files = $_FILES['images'];
        $file_names = $files['name'];
        $type =  array("jpeg","jpg","png");
        if($files['name'][0] != null){
            foreach($file_names as $key => $value){
                $file_types = strtolower(end(explode('.',$value)));
                if(in_array($file_types,$type) == FALSE){
                    $_SESSION['title'] = "Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Only supports upload JPEG or PNG files.',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            foreach($files['error'] as $key => $value){
                if($value != 0){
                    $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Error upload data',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            if(array_sum($files['size']) > 5242880){
                $_SESSION['title'] = "Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'The file size should not be larger than 5MB.',
                })";
                return header("location: ".$_SERVER['REQUEST_URI']);
            }
            foreach($files['tmp_name'] as $key => $value){
                if(is_uploaded_file($value) == FALSE){
                    $_SESSION['title'] = "Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'An error occurred while uploading the file.',
                    })";
                    return header("location: ".$_SERVER['REQUEST_URI']);
                }
            }
            foreach($file_names as $key => $value){
                $images_name =  str_replace("'","\'",$value);
                move_uploaded_file($files['tmp_name'][$key],'../image/'.$value);             
            }   
        }
    }
    execQuery("INSERT INTO products (name,price,category_id,brand_id,description,image,status) VALUES ('$name','$price','$category_id','$brand_id','$description','$image_name','$status')");
    $product = "SELECT * FROM products WHERE name='$name'";
    $id_product = execQuery($product)[0]['id'];
    if(strlen($file_names[0]) > 0){
        foreach($file_names as $key => $value){
            $images_name = str_replace("'","\'",$value);
            $insert_product_images = "INSERT INTO product_images (product_id,image) VALUES ('$id_product','$images_name')";
            execQuery($insert_product_images);
        }
    }
    foreach($sizes_insert as $key => $value){
        $insert_product_sizes = "INSERT INTO product_sizes (size_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_sizes);
    }
    foreach($colors_insert as $key => $value){
        $insert_product_colors = "INSERT INTO product_colors (color_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_colors);
    }
    foreach($styles_insert as $key => $value){
        $insert_product_styles = "INSERT INTO product_styles (style_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_styles);
    }
    $_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Add New Product Success',
        showConfirmButton: false,
        timer: 1500
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">List Products</h3>
				</div>
                <form style="margin: 10px 0px;" class="col-md-6" method="POST" class="navbar-form navbar-left" role="search" enctype="multipart/form-data" style="margin-left: 0px;margin-right: 0px;">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="search" name="search" type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                            </span>
                            <input id="form-search" type="text" value="<?php if (isset($_SESSION['search'])): ?><?php echo $_SESSION['search'] ?><?php endif ?>" name="nhap" class="form-control" placeholder="Search Name">
                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                </form>
                <div class="btn-group col-md-6 row">
                    <?php if (! isset($_GET['search'])): ?>
                        <div class="col-md-6" style="margin: 10px 0px;">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            View Follow Category <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php for($i = 0; $i < count($categories); $i++) {$row = $categories[$i]?>
                                <li><a href="?category=<?php echo $row['name'] ?>"><?php echo $row['name'] ?></a></li>
                                <li role="separator" class="divider"></li>
                            <?php }?>
                        </ul>
                        </div>
                        <div class="col-md-6" style="margin: 10px 0px;">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        View Follow Brand <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php for($i = 0; $i < count($brands); $i++) {$row = $brands[$i]?>
                                <li><a href="?brand=<?php echo $row['name'] ?>"><?php echo $row['name'] ?></a></li>
                                <li role="separator" class="divider"></li>
                            <?php }?>
                        </ul>
                        </div>
                    <?php endif ?>
                </div>
				<div class="panel-body">
					<table class="table table-bordered table-hover table-responsive">
						<thead>
							<tr >
                                <th class="text-center">STT</th>
                                <th class="text-center">Image</th>
								<th class="text-center">Name</th>
								<th class="text-center">Price</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Brand</th>
                       
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach($result as $key=>$value) {?>
                                <tr>
                                    <td class="text-center"><?php echo $key +1?></td>
                                    <td class="text-center"><img style="height: 50px;" src="../image/<?php echo($value['image'])?>" alt=""></td>
                                    <td class="text-center"><?php echo $value['name']?></td>
                                    <td class="text-center" ><?php echo number_format($value['price'],"0",",",".")." VND"?></td>
                                    <td class="text-center">
                                        <?php echo $value['category']?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo $value['brand']?>
                                    </td>
                                   
                                    <td class="text-center"><?php if ($value['status']==1) {?><span class="label label-success">Show</span><?php  }?><?php if ($value['status']==0) {?><span class="label label-default">Hide</span><?php }?></td>
                                    <td class="text-center">
                                        <form action="" method="POST">
                                            <button id="view" name="view"  value="<?php echo $value['id']?>" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</buuton>
                                        </form>
                                    </td>
                                </tr>
              				<?php }?>
						</tbody>
					</table>
                    <div class="row">
                        <div class="col-md-4" >
                            <form action="" method="POST">
                                <button name="new" class="btn btn-default">Add New</button>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <?php if (! isset($_GET['search'])): ?>
                                <?php if ($page > 1): ?>
                                    <div style="text-align: right;">
                                        <ul class="pagination" style="margin: 0px;">
                                            <?php if ($page_cr>1) {?>
                                                <li>
                                                    <a href="product?<?php if (isset($_GET['category'])): ?><?php echo("category=".$_GET['category']."&") ?><?php endif ?><?php if (isset($_GET['brand'])): ?><?php echo("brand=".$_GET['brand']."&") ?><?php endif ?>page=<?php echo $page_cr - 1 ?>">&laquo;</a>
                                                </li>
                                            <?php } ?>
                                            <?php for ($i = 1; $i <= $page ; $i++) {?>
                                            <li id="page<?php echo $i ?>">
                                                <a href="product?<?php if (isset($_GET['category'])): ?><?php echo("category=".$_GET['category']."&") ?><?php endif ?><?php if (isset($_GET['brand'])): ?><?php echo("brand=".$_GET['brand']."&") ?><?php endif ?>page=<?php echo $i ?>"><?php echo $i ?></a>
                                            </li>
                                            <?php } ?>
                                            <?php if ($page_cr<$page) {?>
                                            <li>
                                                <a href="product?<?php if (isset($_GET['category'])): ?><?php echo("category=".$_GET['category']."&") ?><?php endif ?><?php if (isset($_GET['brand'])): ?><?php echo("brand=".$_GET['brand']."&") ?><?php endif ?>page=<?php echo $page_cr + 1 ?>">&raquo;</a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php endif ?>
                            <?php endif ?>
                        </div>
                    </div>
				</div>
			</div>
        </div>
	</div>
</section>
  <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-body">
        <form action="" method="POST" role="form" id="form-cate" enctype= multipart/form-data>
				<div class="row">
                <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Name Product</label>
					<input type="text" class="form-control" id="name" placeholder="Input field" name="name" value="<?php if (isset($_POST['view'])): ?><?php echo $products['name'] ?><?php endif ?>" required>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Category</label>
					<select class="form-control" name="category" required>
                        <option value="">-- Change Category --</option>
                        <?php for($i = 0; $i < count($categories1); $i++) {$category = $categories1[$i]?>
                            <option value="<?= $category['id'] ?>" <?php if (isset($_POST['view'])): ?><?php if ($products['category_id']==$category['id']): ?>selected="selected"<?php endif ?><?php endif ?>><?= $category['name'] ?></option>
                        <?php }?>
                    </select>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Image</label>
                    <input type="file" class="form-control" id="image" placeholder="Input field" name="image" value="">
                    <img style="width: 100%;height: auto;margin-top: 10px" id="img" src="<?php if (isset($_POST['view'])): ?><?php echo ("../image/".$products['image']); ?><?php endif ?>" alt="">
				</div>
                </div>
                <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px;">
                    <label for="">Price</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Input field" id="price" name="price" aria-describedby="basic-addon2" value="<?php if (isset($_POST['view'])): ?><?php echo $products['price'] ?><?php endif ?>" required>
                        <span class="input-group-addon" id="basic-addon2">VND</span>
                    </div>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Brand</label>
					<select class="form-control" name="brand" required>
                        <option value="">-- Change Brand --</option>
                        <?php for($i = 0; $i < count($brands1); $i++) {$brand = $brands1[$i]?>
                            <option value="<?= $brand['id'] ?>" <?php if (isset($_POST['view'])): ?><?php if ($products['brand_id']==$brand['id']): ?>selected="selected"<?php endif ?><?php endif ?>><?= $brand['name'] ?></option>
                        <?php }?>
                    </select>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Description</label>
					<textarea class="form-control" rows="7" name="description"><?php if (isset($_POST['view'])): ?><?php echo $products['description']; ?><?php endif ?></textarea>
				</div>
                </div>
                <div class="col-md-12">
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Images</label>
                    <input type="file" class="form-control" id="images" placeholder="Input field" name="images[]" value="" multiple>
                    <div class="list-images" id="list-images">
                        <?php if (isset($_POST['view'])): ?>
                            <?php foreach ($images as $key => $value) :?>
                                <div>
                                    <img style="width: 100%;height: 100px;" src="../image/<?php echo $value['image']?>" alt="">
                                </div>
                            <?php endforeach ?>	
                        <?php endif ?>
                    </div>
				</div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <p>
                            <b>Sizes</b>
                        </p>
                        <div class="list-size" style="">
                            <?php for($i = 0; $i < count($sizes1); $i++) {$size = $sizes1[$i]?>
                                <div class="size-item">
                                    <input type="checkbox" name="size[]" class="size" <?php if (isset($_POST['view'])): ?><?php if (in_array($size['id'],$check_size)) {?>checked="checked"<?php }?><?php endif ?> value="<?= $size['id'] ?>">
                                    <label class="size-label" for=""><?= $size['size'] ?></label>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <p>
                            <b>Colors</b>
                        </p>
                        <div class="list-color" style="">
                            <?php for($i = 0; $i < count($colors1); $i++) {$color = $colors1[$i]?>
                                <div class="color-item">
                                    <input type="checkbox" name="color[]" class="color" <?php if (isset($_POST['view'])): ?><?php if (in_array($color['id'],$check_color)) {?>checked="checked"<?php }?><?php endif ?> value="<?= $color['id'] ?>">
                                    <label style="padding: 0px 10px;" for="" class="color-label"><?= $color['name'] ?></label>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <p>
                            <b>Styles</b>
                        </p>
                        <div class="list-style" style="">
                            <?php for($i = 0; $i < count($styles1); $i++) {$style = $styles1[$i]?>
                                <div class="style-item">
                                    <input type="checkbox" name="style[]" class="style" <?php if (isset($_POST['view'])): ?><?php if (in_array($style['id'],$check_style)) {?>checked="checked"<?php }?><?php endif ?> value="<?= $style['id'] ?>">
                                    <label style="padding: 0px 10px;" for="" class="style-label"><?= $style['name'] ?></label>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <p>
                            <b>Status</b>
                        </p>
                        <div style="font-size: 20px;">
                            <input type="radio" name="status" id="status" <?php if (isset($_POST['view'])): ?><?php if ($products['status']==0) {?>checked="checked"<?php }?><?php endif ?> value="0" required>
                            <label for="" class="label label-danger">Hide</label>
                            <input type="radio" name="status" id="status" <?php if (isset($_POST['view'])): ?><?php if ($products['status']==1) {?>checked="checked"<?php }?><?php endif ?> value="1" required>
                            <label for="" class="label label-success">Show</label>
                        </div>
                    </div>
                </div>
                </div>
                <div class="button" style="margin-top: 15px;">
                    <button id="submit" type="submit" value="<?php if (isset($_POST['view'])): ?><?php echo $products['id'] ?><?php endif ?>" name="add" class="btn btn-primary">Add New</button>
                    <a id="close" href="<?php echo $_SERVER['REQUEST_URI']; ?>" name="close" class="btn btn-default">Close</a>
                </div>
		</form>
        </div>
        <div class="modal-footer">
        </div>
        </div>
    </div>
</div>
<?php if (isset($_POST['new'])): ?>
    <script>
        $(document).ready(function(){
            $('#Modal').modal('show');
            $(".size").prop('required', true);
            $(".color").prop('required', true);
            $(".style").prop('required', true);
            $("#image").prop('required', true);
        });
        $(document).on("change",".size, .color, .style", function(){
            if ($(".size:checked").length == 0) {
                $(".size").prop('required', true);
            } else {
                $(".size").prop('required', false);
            }
            if ($(".color:checked").length == 0) {
                $(".color").prop('required', true);
            } else {
                $(".color").prop('required', false);
            }
            if ($(".style:checked").length == 0) {
                $(".style").prop('required', true);
            } else {
                $(".style").prop('required', false);
            }
        });
    </script>
<?php endif ?>
<?php if (isset($_POST['view'])): ?>
    <script>
        $(document).ready(function(){
            $('#Modal').modal('show');
            $('#submit').text('Update');
            $('#submit').attr('name', 'update');
        });
        $(document).on("change",".size, .color, .style", function(){
            if ($(".size:checked").length == 0) {
                $(".size").prop('required', true);
            } else {
                $(".size").prop('required', false);
            }
            if ($(".color:checked").length == 0) {
                $(".color").prop('required', true);
            } else {
                $(".color").prop('required', false);
            }
            if ($(".style:checked").length == 0) {
                $(".style").prop('required', true);
            } else {
                $(".style").prop('required', false);
            }
        });
    </script>
<?php endif ?>
<?php
    include 'footer.php';
?>