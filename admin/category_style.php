<?php
include '../connect.php';
if(isset($_POST['name'])){
	$name = $_POST['name'];
	$style = "INSERT INTO styles(name)VALUES('$name')";
	$style_add = execQuery($style);
}
$select_style = "SELECT * FROM styles";
$select_styles = execQuery($select_style);
include 'header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
				</div>
				<div class="panel-body">
					<form action="" method="POST" role="form">
						<legend>Add Style</legend>
						<div class="form-group">
							<input type="text" name="name" id="" placeholder="Enter Style!" class="form-control">
						</div>
						<button type="submit" class="btn btn-primary">Add</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Information Style</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th style="text-align: center">STT</th>
									<th style="text-align: center">Name Style</th>
									<th style="text-align: center">Option</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($select_styles as $key => $value) : ?>
								<tr>
									<td style="text-align: center"><?php echo $key+1 ?></td>
									<td style="text-align: center"><?php echo $value["name"]?></td>
									<td style="text-align: center">
									<a href="edit_style.php?id=<?php echo $value['id']?>" class="btn btn-success">Edit</a>
									<a href="delete_style.php?id=<?php echo $value['id']?>" class="btn btn-danger">Delete</a>
									</td>
								</tr>
							<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  
include 'footer.php';
?>