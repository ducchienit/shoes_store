<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$sql = "SELECT * FROM customers";
$row = execQuery($sql);
$total = count($row);
$limit = 5;
$page = ceil($total/$limit);
$page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
$start = ($page_cr - 1)*$limit;
$sql_account = "SELECT customers.*, accounts.email, accounts.is_admin, orders.id as order_id  
FROM customers 
INNER JOIN accounts ON customers.account_id = accounts.id
LEFT JOIN orders ON customers.account_id = orders.account_id
GROUP BY customers.account_id LIMIT $start,$limit";
$result = execQuery($sql_account);
if(isset($_POST['remove'])){
    $id = $_POST['remove'];
    $account = execQuery("SELECT * FROM accounts WHERE id = '$id'");
    $order = execQuery("SELECT * FROM orders WHERE account_id ='$id'");
	if(count($order) > 0 || $account[0]['is_admin'] == 1){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'Can not remove',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
    execQuery("DELETE FROM customers WHERE account_id = '$id'");
    execQuery("DELETE FROM accounts WHERE id = '$id'");
    $_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Deleted successfully',
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['view'])){
    $id = $_POST['view'];
    $info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.id = '$id'")[0];
}
if(isset($_POST['nhap'])){
    header('location: account?search='.$_POST['nhap']);
}
if(isset($_GET['search'])){
    if($_GET['search'] == ""){
        header('location: account');
    }
    $name_search =  $_GET['search'];
    $sql = "SELECT customers.*, accounts.email, accounts.is_admin, orders.id as order_id  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    LEFT JOIN orders ON customers.account_id = orders.account_id
    WHERE (full_name LIKE '%$name_search%' OR email LIKE '%$name_search%') GROUP BY customers.account_id";
    $result = execQuery($sql);
    $_SESSION['search'] = $_GET['search'];
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">List Accounts</h3>
				</div>
                <form method="POST" class="navbar-form navbar-left" role="search" enctype="multipart/form-data" style="margin-left: 0px;margin-right: 0px;">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="search" name="search" type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                            </span>
                            <input id="form-search" type="text" value="<?php if (isset($_SESSION['search'])): ?><?php echo $_SESSION['search'] ?><?php endif ?>" name="nhap" class="form-control" placeholder="Search Username Or Email">
                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
				</form>
				<div class="panel-body">
					<table class="table table-bordered table-hover table-responsive">
						<thead>
							<tr >
								<th class="text-center">STT</th>
								<th class="text-center">Account Name</th>
								<th class="text-center">Email</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
              				<?php for($i = 0; $i < count($result); $i++) {$row = $result[$i]?>
							<tr>
								<td style="padding: 21px 0px;" class="text-center"><?php echo $i +1?></td>
								<td>
                                    <div style="float: left;">
                                        <img alt="Overview" src="../image/<?php if($row['avatar'] != NULL)  echo $row['avatar']?><?php else echo("default.png") ?>" align="middle" border="0" height="50" width="50" style="border-radius: 50%;box-shadow: 0px 0px 1px;">
                                        <span style="margin: 0px 0px 0px 10px;" class="iconLabel"><?php echo $row['full_name'] ?></span>
                                    </div>
                                </td>
                                <td style="padding: 21px 0px;" class="text-center"><?php echo $row['email']?></td>
                                <td style="padding: 21px 0px;" class="text-center"><?php if ($row['is_admin']==1) {?><span class="label label-success">Admin</span><?php  }?><?php if ($row['is_admin']==0) {?><span class="label label-default">Customer</span><?php }?></td>
								<td style="padding: 21px 0px;" class="text-center">
                  					<form action="" method="POST" id="remove">
                                        <button type="submit" onclick="removeAccount()" name="remove" value="<?php echo($row['account_id']); ?>" <?php if ($row['is_admin']==1 || !empty($row['order_id'])) {?><?php echo("disabled"); ?><?php  }?> class="btn btn-danger btn-xs">Remove</button>
                                        <button type="submit" name="view" value="<?php echo($row['id']); ?>" class="btn btn-primary btn-xs">View Info</button>
                                    </form>
								</td>
							</tr>
              				<?php }?>
						</tbody>
					</table>
                    <?php if (! isset($_GET['search'])): ?>
                        <?php if ($page > 1): ?>
                            <div style="text-align: right;">
                                <ul class="pagination" style="margin: 0px;">
                                    <?php if ($page_cr>1) {?>
                                    <li><a href="account?page=<?php echo $page_cr - 1 ?>">&laquo;</a></li>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= $page ; $i++) {?>
                                    <li id="page<?php echo $i ?>"><a href="account?page=<?php echo $i ?>"><?php echo $i ?></a></li>
                                    <?php } ?>
                                    <?php if ($page_cr<$page) {?>
                                    <li><a href="account?page=<?php echo $page_cr + 1 ?>">&raquo;</a></li>
                                    <?php } ?>
                                </ul>
			                </div>
                        <?php endif ?>
				    <?php endif ?>
				</div>
			</div>
        </div>
	</div>
</section>
<div class="modal fade" id="view" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>
        <h4 class="modal-title">Profile</h4>
      </div>
      <div class="modal-body">
      <div class="row" style="margin: 15px 15px;">
        <div class="col-md-12">
            <div class="thumbnail row" style="padding-top: 20px;">
                    <div class="col-md-3" style="text-align: center;">
                        <h4>Profile Picture</h4>
                        <img id="avatar" style="box-shadow: 0px 0px 30px #ccc;margin: 15px 0px;height: 180px;width: 170px;border-radius: 50%;" src="../image/<?php if($info['avatar'] != null)  echo $info['avatar'] ?><?php else echo("default.png") ?>" alt="...">
                    </div>
                    <div class="caption col-md-9">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Full Name</label>
                                <input type="text" name="name" value="<?php echo $info['full_name'] ?>" class="form-control" id="" placeholder="" disabled>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Email Address</label>
                                <input type="email" name="email" value="<?php echo $info['email'] ?>" class="form-control" id="" placeholder="" disabled>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Phone Number</label>
                                <input type="text" name="phone" value="<?php echo $info['phone'] ?>" class="form-control" id="" placeholder="" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Birth Of Date</label>
                                <input type="date" name="birth" value="<?php echo $info['birth'] ?>" class="form-control" id="" placeholder="" disabled>
                            </div>
                            <div class="form-group" style="">
                                <p>
                                    <b>Gender</b>
                                </p>
                                <input type="radio" name="gender" id="male" value="0" <?php if (isset($info['gender'])): ?><?php if ($info['gender']==0) {?>checked="checked"<?php }?><?php endif ?> required>
                                <label for="gender">Male</label>
                                <input type="radio" name="gender" id="female" value="1" <?php if (isset($info['gender'])): ?><?php if ($info['gender']==1) {?>checked="checked"<?php }?><?php endif ?> required>
                                <label for="gender">Female</label>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Address</label>
                                <textarea class="form-control" id="address" name="address" rows="3" disabled><?php echo $info['address'] ?></textarea>
                            </div>
                        </div>
                        </div>
                    </div>
            </div>
        </div>
	</div>
      </div>
      <div class="modal-footer">
        <a  href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-default">Close</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php if (isset($_POST['view'])): ?>
    <script>
        $(document).ready(function(){
            $('#view').modal('show');
        });
    </script>
<?php endif ?>
<script>
    $(document).ready(function(){
        $("#form-search").hide();
        $("#search").click(function(){
            $("#form-search").toggle("");
        });
    });
    function $_GET(param) {
        var vars = {};
        window.location.href.replace( location.hash, '' ).replace( 
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;	
        }
        return vars;
    }
    var name = $_GET('page');
    if(name == "null"){
        document.getElementById("page1").classList.add("active");
    }
    document.getElementById("page"+name).classList.add("active");
</script>
<?php
    include 'footer.php';
?>