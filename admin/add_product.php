<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$sql_category = "SELECT * FROM categories";
$sql_brand = "SELECT * FROM brands";
$sql_size = "SELECT * FROM sizes";
$sql_color = "SELECT * FROM colors";
$sql_style = "SELECT * FROM styles";
$categories = execQuery($sql_category);
if(count($categories) == 0){ };
$brands = execQuery($sql_brand);
if(count($brands) == 0){ };
$sizes = execQuery($sql_size);
if(count($sizes) == 0){ };
$colors = execQuery($sql_color);
if(count($colors) == 0){ };
$styles = execQuery($sql_style);
if(count($styles) == 0){ };
if(isset($_POST['add'])){
    $name = $_POST['name'];
    $product = "SELECT * FROM products WHERE name='$name'";
    $check = execQuery($product);
    if($check != null){
        return header("location: add_product.php");
    }
    $price = str_replace(".","",$_POST['price']);
    $category_id = $_POST['category'];
    $brand_id = $_POST['brand'];
    $description = $_POST['description'];
    $status = $_POST['status'];
    $sizes_insert = $_POST['size'];
    $colors_insert = $_POST['color'];
    $styles_insert = $_POST['style'];
    if (isset($_FILES['image'])) {
		$file = $_FILES['image'];
		$file_name = $file['name'];
		move_uploaded_file($file['tmp_name'],'images/'.$file_name);
    }
    if (isset($_FILES['images'])) {
		$files = $_FILES['images'];
		$file_names = $files['name'];
        foreach($file_names as $key => $value){
            move_uploaded_file($files['tmp_name'][$key],'images/'.$value);
        }
    }
    $insert_product = "INSERT INTO products (name,price,category_id,brand_id,description,image,status) VALUES ('$name','$price','$category_id','$brand_id','$description','$file_name','$status')";
    $result = execQuery($insert_product);
    $product = "SELECT * FROM products WHERE name='$name'";
    $id_product = execQuery($product)[0]['id'];
    if(strlen($file_names[0]) > 0){
        foreach($file_names as $key => $value){
            $insert_product_images = "INSERT INTO product_images (product_id,image) VALUES ('$id_product','$value')";
            execQuery($insert_product_images);
        }
    }
    foreach($sizes_insert as $key => $value){
        $insert_product_sizes = "INSERT INTO product_sizes (size_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_sizes);
    }
    foreach($colors_insert as $key => $value){
        $insert_product_colors = "INSERT INTO product_colors (color_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_colors);
    }
    foreach($styles_insert as $key => $value){
        $insert_product_styles = "INSERT INTO product_styles (style_id,product_id) VALUES ('$value','$id_product')";
        execQuery($insert_product_styles);
    }
    
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title" id="form-title">Add New Product</h3>
				</div>
				<div class="panel-body">
				<form action="" method="POST" role="form" id="form-cate" enctype= multipart/form-data>
				<div class="row">
                <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Name Product</label>
					<input type="text" class="form-control" id="name" placeholder="Input field" name="name" value="" required>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Category</label>
					<select class="form-control" name="category" required>
                        <option disabled selected value="">-- Change Category --</option>
                        <?php for($i = 0; $i < count($categories); $i++) {$category = $categories[$i]?>
                            <option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                        <?php }?>
                    </select>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Image</label>
                    <input type="file" class="form-control" id="image" placeholder="Input field" name="image" value="" required>
                    <img style="width: 100%;height: auto;margin-top: 10px" id="img" src="" alt="">
				</div>
                </div>
                <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px;">
                    <label for="">Price</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Input field" id="price" name="price" aria-describedby="basic-addon2" required>
                        <span class="input-group-addon" id="basic-addon2">VND</span>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Brand</label>
					<select class="form-control" name="brand" required>
                        <option disabled selected value="">-- Change Brand --</option>
                        <?php for($i = 0; $i < count($brands); $i++) {$brand = $brands[$i]?>
                            <option value="<?= $brand['id'] ?>"><?= $brand['name'] ?></option>
                        <?php }?>
                    </select>
				</div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Description</label>
					<textarea class="form-control" rows="7" name="description"></textarea>
				</div>
                </div>
                <div class="col-md-12">
                <div class="form-group" style="margin-bottom: 15px;">
					<label for="">Images</label>
                    <input type="file" class="form-control" id="images" placeholder="Input field" name="images[]" value="" multiple>
                    <div class="list-images" id="list-images"></div>
				</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <p>
                            <b>Size</b>
                        </p>
                        <div style="font-size: 20px;">
                            <?php for($i = 0; $i < count($sizes); $i++) {$size = $sizes[$i]?>
                                <input type="checkbox" name="size[]" id="size" value="<?= $size['id'] ?>" required>
                                <label for="" class="label label-primary"><?= $size['size'] ?></label>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <p>
                            <b>Color</b>
                        </p>
                        <div style="text-shadow: 0px 0px 1px #000000;font-size: 20px;">
                            <?php for($i = 0; $i < count($colors); $i++) {$color = $colors[$i]?>
                                <input type="checkbox" name="color[]" id="color" value="<?= $color['id'] ?>" required>
                                <label for="" class="label"><input type="color" value="<?= $color['name'] ?>" disabled></label>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <p>
                            <b>Style</b>
                        </p>
                        <div style="font-size: 20px;">
                            <?php for($i = 0; $i < count($styles); $i++) {$style = $styles[$i]?>
                                <input type="checkbox" name="style[]" id="style" value="<?= $style['id'] ?>" required>
                                <label for="" class="label label-default"><?= $style['name'] ?></label>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <p>
                            <b>Status</b>
                        </p>
                        <div style="font-size: 20px;">
                            <input type="radio" name="status" id="status" value="0" required>
                            <label for="" class="label label-danger">Hide</label>
                            <input type="radio" name="status" id="status" value="1" required>
                            <label for="" class="label label-success">Show</label>
                        </div>
                    </div>
                </div>
                </div>
				<button id="submit" type="submit" name="add" class="btn btn-default">Add New</button>
			    </form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    document.getElementById("image").addEventListener("change", myFunction);
    function myFunction(event) {
            var value = document.getElementById('image').value;
            if(value == ""){
                document.getElementById('img').src = "";
            }else{
                var reader = new FileReader();
                var output = document.getElementById('img');
                reader.onload = function(){
                    output.src = reader.result;
                }
                reader.readAsDataURL(event.target.files[0]);
            }
    }
    document.getElementById("images").addEventListener("change", images);
    function images(evt) {
        var value = document.getElementById('images').value;
        if(value != "" || value == ""){
            document.getElementById('list-images').innerHTML = " ";
        }
        var files = evt.target.files;

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
            // Render thumbnail.
            var div = document.createElement('div');
            div.innerHTML = 
            [
                '<img style="width: 100%;height: 100px;" src="', 
                e.target.result,
                '" title="', escape(theFile.name), 
                '"/>'
            ].join('');
            
            document.getElementById('list-images').insertBefore(div, null);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        }
    }
    $(document).on("click","input:checkbox[id='size']", function(){
        var size = $("input:checkbox[id='size']");
        var checked = $("input:checkbox[id='size']:checked").length;
        if (checked == 0) {
            size.prop('required', true);
        } else {
            size.prop('required', false);
        }
    });
    $(document).on("click","input:checkbox[id='color']", function(){
        var color = $("input:checkbox[id='color']");
        var checked = $("input:checkbox[id='color']:checked").length;
        if (checked == 0) {
            color.prop('required', true);
        } else {
            color.prop('required', false);
        }
    });
    $(document).on("click","input:checkbox[id='style']", function(){
        var style = $("input:checkbox[id='style']");
        var checked = $("input:checkbox[id='style']:checked").length;
        if (checked == 0) {
            style.prop('required', true);
        } else {
            style.prop('required', false);
        }
    });
    function formatNumberField() {
    // unformat the value
    var value = this.value.replace(/[^\d,]/g, '');

    // split value into (leading digits, 3*x digits, decimal part)
    // also allows numbers like ',5'; if you don't want that,
    // use /^(\d{1,3})((?:\d{3})*))((?:,\d*)?)$/ instead
    var matches = /^(?:(\d{1,3})?((?:\d{3})*))((?:,\d*)?)$/.exec(value);

    if (!matches) {
        // invalid format; deal with it however you want to
        // this just stops trying to reformat the value
        return;
    }

    // add a space before every group of three digits
    var spaceified = matches[2].replace(/(\d{3})/g,'.$1');

    // now splice it all back together
    this.value = [matches[1], spaceified, matches[3]].join('');
    }
    document.getElementById('price').onkeyup = formatNumberField;
</script>
<?php
    include 'footer.php';
?>