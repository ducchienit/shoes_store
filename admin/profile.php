<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
if(isset($_SESSION['admin'])){
    $full_name = $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$full_name'")[0];
}
if(isset($_POST['update'])){
    $id = $_POST['update'];
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$address = $_POST['address'];
	$email = $_POST['email'];
	$birth = $_POST['birth'];
	$gender = $_POST['gender'];
	if(isset($_FILES['image'])){
		$file = $_FILES['image'];
		$file_name = $file['name'];
	}
	execQuery("UPDATE accounts SET email='$email' WHERE id='$id'");
	if($file_name != null){
        $_SESSION['avatar'] = $file_name;
		execQuery("UPDATE customers SET full_name='$name', avatar='$file_name', phone='$phone', gender='$gender', birth='$birth', address='$address' WHERE account_id = '$id'");
		move_uploaded_file($file['tmp_name'],'image/'.$file_name);
	}else{
		execQuery("UPDATE customers SET full_name='$name', phone='$phone', gender='$gender', birth='$birth', address='$address' WHERE account_id = '$id'");
	}
    $_SESSION['admin'] = $name;
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Update Profile Success',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="margin: 15px 15px;">
        <div class="col-md-12">
            <div class="thumbnail row" style="padding-top: 20px;">
                <form action="" method="POST" enctype= multipart/form-data>
                    <div class="col-md-3" style="text-align: center;">
                        <h4>Profile Picture</h4>
                        <img id="avatar" style="box-shadow: 0px 0px 30px #ccc;margin: 15px 0px;height: 215px;width: 200px;border-radius: 50%;" src="../image/<?php if(isset($info) && strlen($info['avatar']) > 0)  echo $info['avatar'] ?><?php else echo("default.png") ?>" alt="...">
                        <img id="avatar_upload" style="display: none;box-shadow: 0px 0px 30px #ccc;margin: 15px 0px;height: 215px;width: 200px;border-radius: 50%;" src="" alt="...">
                        <div>
                            <label class="custom-file-upload">
                                <input type="file" name="image" id="image" class="form-control" style="display: none;">
                                <a id="upload" name="upload_image" class="btn btn-default" role="">Upload Avatar</a>
                            </label>
                        </div>
                    </div>
                    <div class="caption col-md-9">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Full Name</label>
                                <input type="text" name="name" value="<?php echo $info['full_name'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Email Address</label>
                                <input type="email" name="email" value="<?php echo $info['email'] ?>" class="form-control" id="" placeholder="" required>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Phone Number</label>
                                <input type="text" name="phone" value="<?php echo $info['phone'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Birth Of Date</label>
                                <input type="date" name="birth" value="<?php echo $info['birth'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                            <div class="form-group" style="">
                                <p>
                                    <b>Gender</b>
                                </p>
                                <input type="radio" name="gender" id="male" value="0" <?php if (isset($info['gender'])): ?><?php if ($info['gender']==0) {?>checked="checked"<?php }?><?php endif ?> required>
                                <label for="gender">Male</label>
                                <input type="radio" name="gender" id="female" value="1" <?php if (isset($info['gender'])): ?><?php if ($info['gender']==1) {?>checked="checked"<?php }?><?php endif ?> required>
                                <label for="gender">Female</label>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label for="">Address</label>
                                <textarea class="form-control" id="address" name="address" rows="3"><?php echo $info['address'] ?></textarea>
                            </div>
                        </div>
                        </div>
                        <button type="submit" value="<?php echo $info['account_id'] ?>" name="update" class="btn btn-primary" role="button">Save</button>
                        <a href="?GET=<?php echo('password')?>" class="btn btn-danger">Change Password</a>
                    </div>
                </form>
            </div>
        </div>
	</div>
</section>
<script>
    document.getElementById("image").addEventListener("change", myFunction);
    var img = "";
    function myFunction(event) {
        console.log(event);
        img = document.getElementById("image").value;
        var path = img.slice(12);
        if(img != ""){
            document.getElementById("avatar").style.display = "none";
            document.getElementById("avatar_upload").style.display = "";
            var reader = new FileReader();
            var output = document.getElementById('avatar_upload');
            reader.onload = function(){
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }else{
            document.getElementById("avatar").style.display = "";
            document.getElementById("avatar_upload").style.display = "none";
        }
    }
</script>
<?php
    include 'footer.php';
?>