<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$sql = "SELECT * FROM categories"; 
$result = execQuery($sql);
if (isset($_POST['add'])) {
	$check = execQuery("SELECT * FROM categories");
    $not_name = str_replace(" ","",$_POST['name']);
    $category_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['name']);
        array_push($category_array,$name_value);
	}
	if(in_array($not_name,$category_array)){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'This category already exists',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
  	$name = $_POST['name'];
  	$sql_insert = "INSERT INTO categories (name) VALUES ('$name')";
  	execQuery($sql_insert);
  	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Add New Category Success',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['remove'])){
	$id = $_POST['remove'];
    $product = execQuery("SELECT * FROM products WHERE category_id ='$id'");
	if(count($product) > 0){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'Can not remove',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
	$delete = "DELETE FROM categories WHERE id = '$id'";
    execQuery($delete);
    $_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Deleted successfully',
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['edit'])){
	$id = $_POST['edit'];
  	$sql_category = "SELECT * FROM categories WHERE id = '$id'";
	$edit =  execQuery($sql_category);
	$row = $edit[0];
}
if(isset($_POST["update"])){
	$id = $_POST["update"];
	$check = execQuery("SELECT * FROM categories EXCEPT SELECT * FROM categories WHERE id = '$id'");
    $not_name = str_replace(" ","",$_POST['name']);
    $category_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['name']);
        array_push($category_array,$name_value);
	}
	if(in_array($not_name,$category_array)){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'This category already exists',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
    $name = $_POST['name'];
    $update = "UPDATE categories SET name='$name' WHERE id = '$id'";
    execQuery($update);
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Update Category Success',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title" id="form-title">Add New Category</h3>
				</div>
				<div class="panel-body">
				<form action="" method="POST" role="form" id="form-cate">
				<div class="form-group">
					<label for="">Name Category</label>
					<input type="text" class="form-control" id="name" placeholder="Input field" name="name" value="<?php if (isset($_POST['edit'])): ?><?php echo $row['name'] ?><?php endif ?>" required>
				</div>

				<button id="submit" type="submit" name="add" value="<?php if (isset($_POST['edit'])): ?><?php echo $row['id'] ?><?php endif ?>" class="btn btn-primary">Add</button>
        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" style="display: none;" type="" id="cancel" name="cancel" class="btn btn-danger" onclick="cancel()">Cancel</a>
			</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">List Categories</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered table-hover">
						<thead>
							<tr >
								<th style="text-align: center">STT</th>
								<th style="text-align: center">Name Category</th>
								<th style="text-align: center">Action</th>
							</tr>
						</thead>
						<tbody>
              				<?php for($i = 0; $i < count($result); $i++) {$row = $result[$i]?>
							<tr>
								<td style="text-align: center"><?php echo $i +1?></td>
								<td><?php echo $row['name']?></td>
								<td style="text-align: center">
									<form action="" method="POST">
										<button type="submit" name="edit" value="<?php echo $row['id']?>" class="btn btn-success btn-xs">Edit</button>
										<button type="submit" name="remove" value="<?php echo $row['id']?>" class="btn btn-danger btn-xs">Remove</button>
									</form>
								</td>
							</tr>
              				<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if (isset($_POST['edit'])): ?>
	<script>
    document.getElementById("form-title").innerText = "Edit Category";
    document.getElementById("submit").setAttribute("name", "update");
    document.getElementById("submit").innerText = "Update";
    document.getElementById("form-cate").lastElementChild.style.display = "";
  </script>
<?php endif ?>
<?php
    include 'footer.php';
?>