<?php
  if(isset($_GET['GET'])){
    if($_GET['GET'] == "category"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: category");
    }
    if($_GET['GET'] == "password"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: password");
    }
    if($_GET['GET'] == "brand"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: brand");
    }
    if($_GET['GET'] == "order"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: order");
    }
    if($_GET['GET'] == "style"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: style");
    }
    if($_GET['GET'] == "product"){
      $_SESSION['search'] = "";
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: product");
    }
    if($_GET['GET'] == "color"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: color");
    }
    if($_GET['GET'] == "index"){
      $_SESSION['GET'] = null;
      return header("location: home");
    }
    if($_GET['GET'] == "account"){
      $_SESSION['search'] = "";
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: account");
    }
    if($_GET['GET'] == "size"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: size");
    }
    if($_GET['GET'] == "profile"){
      $_SESSION['GET'] = $_GET['GET'];
      return header("location: profile");
    }
    if($_GET['GET'] == "logout"){
      unset($_SESSION['admin']);
	    return header("location: ../login.php");
    }
  }
  $order = count(execQuery("SELECT * FROM orders WHERE status='0'"));
  $day = date('Y-m-d');
  $order_day = count(execQuery("SELECT * FROM orders WHERE status='0' AND created_at LIKE '$day%'"));
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/AdminLTE.css">
  <link rel="stylesheet" href="css/_all-skins.min.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/angular.min.js"></script>
  <script src="js/app.js"></script>
  <script src="js/chart.js@2.8.0"></script>
  <script src="js/Chart.min.js"></script>
  <script src="js/utils.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="?GET=<?php echo('index')?>" class="logo" style="background-color: #367fa9;">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">SYSTEM MANAGE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color: #3c8dbc;">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="notifications-menu">
            <a href="../home.php">FRONT END <i class="fa fa-font-awesome" aria-hidden="true"></i></a>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php if($order > 0){ echo $order; }?></span>
            </a>
            <?php if($order >0) {?>
              <ul class="dropdown-menu">
              <li class="header">You have <?php echo $order ?> orders to confirm</li>
              <?php if($order_day > 0) {?>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="order?order_day=<?php echo $day ?>">
                      <i class="fa fa-shopping-cart text-aqua"></i><?php echo $order_day ?> new orders today
                    </a>
                  </li>
                </ul>
              </li>
              <?php }?>
              <li class="footer"><a href="order?status=pending">View all</a></li>
            </ul>
            <?php }?>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../image/<?php if(isset($_SESSION) && strlen($_SESSION['avatar']) > 0)  echo $_SESSION['avatar'] ?><?php else echo("default.png") ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['admin'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../image/<?php if(isset($_SESSION) && strlen($_SESSION['avatar']) > 0)  echo $_SESSION['avatar'] ?><?php else echo("default.png") ?>" class="img-circle" alt="User Image">
                <p>Administrator</p>
                <p><?php echo $_SESSION['admin'] ?></p>
                
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="?GET=<?php echo('profile')?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="?GET=<?php echo('logout')?>" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../image/<?php if(isset($_SESSION) && strlen($_SESSION['avatar']) > 0)  echo $_SESSION['avatar'] ?><?php else echo("default.png") ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['admin'] ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i>
            <span>Product Attributes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="?GET=<?php echo('size')?>"><i class="fa fa-sort-numeric-asc"></i>Manage Size</a></li>
            <li><a href="?GET=<?php echo('color')?>"><i class="fa fa-star-half-o"></i>Manage Color</a></li>
            <li><a href="?GET=<?php echo('style')?>"><i class="fa fa-diamond"></i>Manage Style</a></li>
          </ul>
        </li>
        <li>
          <a href="?GET=<?php echo('category')?>">
            <i class="fa fa-table"></i> <span>Manage Categories</span>
          </a>
        </li>

        <li class="treeview">
         
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>

        
        <li>
          <a href="?GET=<?php echo('brand')?>">
            <i class="fa fa-audio-description"></i> <span>Manage Brand</span>
          </a>
        </li>
        <li>
          <a href="?GET=<?php echo('account')?>">
            <i class="fa fa-users"></i> <span>List Accounts</span>
          </a>
        </li>
        <li>
          <a href="?GET=<?php echo('product')?>">
            <i class="fa fa-product-hunt"></i><span>Manage Product</span>
          </a>
        </li>
        <li>
          <a href="?GET=<?php echo('order')?>">
            <i class="fa fa-shopping-cart"></i><span>Manage Order</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="?GET=<?php echo('index')?>">Home</a></li>
        <?php if(isset($_SESSION['GET']) && $_SESSION['GET'] != null) {?>
          <li><a href="<?php echo($_SESSION['GET'])?>"><?php if (isset($_SESSION['GET'])): ?><?php echo($_SESSION['GET'])?><?php endif ?></a></li>
        <?php }?>
      </ol>
    </section>
