<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
    return header("location: ../index.php");
}
$order = count(execQuery("SELECT * FROM orders WHERE status='0'"));
$customer = count(execQuery("SELECT * FROM accounts WHERE is_admin!='1'"));
$product = count(execQuery("SELECT * FROM products WHERE status!='0'"));
$total_revenue = null;
$total = execQuery("SELECT * FROM orders WHERE status = 2");
foreach($total as $key => $value){
  $total_revenue += $value['total_price'];
}
$date=[];
$date_sql=[];

for($i=date('j') - date('w')+1;$i<=(date('j') - date('w')+7);$i++){
  // echo($i.date(' F Y'))."<br>";
  array_push($date,$i.date('-m-Y'));
  array_push($date_sql,date('Y-m-').$i);
}
$year_array = [];
for($i=2019;$i<=date('Y');$i++){
  array_push($year_array,$i);
}
if(isset($_POST['view'])){
  if($_POST['month']==null){
    unset($_POST['month']);
  }
  if($_POST['week']==null){
    unset($_POST['week']);
  }
  if(isset($_POST['month'])){
    if(isset($_SESSION['month'])){
      if($_POST['month'] != $_SESSION['month']){
        unset($_POST['week']);
      }
    }
    $month_num =$_POST['month'];
    $_SESSION['month'] = $month_num; 
    $month_name = date("F", mktime(0, 0, 0, $month_num, 10));
    $year_name = $_POST['year']; 
    $textdt="$month_name $year_name";
    $dt= strtotime( $textdt);
    $currdt=$dt;
    $nextmonth=strtotime($textdt."+1 month");
    $i=0;
    $weeks = [];
    do{
        $weekday= date("w",$currdt);
        $nextday=6-$weekday;
        $endday=abs($weekday-7);
        $startarr[$i]=$currdt;
        $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
        $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");
        array_push($weeks,array("value"=>date("Y-m-d",$startarr[$i])."/".date("Y-m-d",$endarr[$i]),"week"=>date("d F Y",$startarr[$i])." - ".date("d F Y",$endarr[$i])));
        $i++;
    }while($endarr[$i-1]<$nextmonth);
  }
  if(isset($_POST['week'])){
    function displayDates($date1, $date2, $format = 'Y-m-d' ) {
      $dates = array();
      $current = strtotime($date1);
      $date2 = strtotime($date2);
      $stepVal = '+1 day';
      while( $current <= $date2 ) {
         $dates[] = date($format, $current);
         $current = strtotime($stepVal, $current);
      }
      return $dates;
    }
    $start = date_format(date_create(explode("/",$_POST['week'])[0]),"d F Y");
    $end = date_format(date_create(explode("/",$_POST['week'])[1]),"d F Y");
    $week = displayDates(explode("/",$_POST['week'])[0], explode("/",$_POST['week'])[1]);
  }

}
// echo("<pre>");
// print_r($date_sql);
// die();
include 'header.php'
?>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo number_format($order,"0","",",") ?></h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </div>
            <a href="order?status=pending" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo number_format($total_revenue,"0","",".") ?><sup style="font-size: 20px"> VND</sup></h3>

              <p>Total Revenue</p>
            </div>
            <div class="icon">
            <i class="fa fa-money" aria-hidden="true"></i>
            </div>
            <a href="home" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo number_format($customer,"0","",",") ?></h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
            <i class="fa fa-users" aria-hidden="true"></i>
            </div>
            <a href="?GET=account" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo number_format($product,"0","",",") ?></h3>

              <p>Products Show</p>
            </div>
            <div class="icon">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
            </div>
            <a href="?GET=product" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Report Order Statistics</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="canvas" style="display: block; height: 468px; width: 936px;" width="1872" height="936" class="chartjs-render-monitor"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Option</h3>
            </div>
            <div class="box-body">
                <form method="POST" action="">
                <div class="form-group">
                  <label>Year</label>
                  <select class="form-control" id="year" name="year" required>
                    <option value="" disabled selected>-- Change Year --</option>
                    <?php foreach($year_array as $value) {?>
                    <option <?php if (isset($_POST['view'])): ?><?php if ($_POST['year']== $value): ?>selected="selected"<?php endif ?><?php endif ?> value="<?php echo $value ?>"><?php echo $value ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Month</label>
                  <select class="form-control" id="month" name="month" onchange="let x = document.getElementById('month').value;console.log(x);">
                    <option value="" selected>-- Change Month --</option>
                    <?php if (isset($_POST['year'])): ?>
                      <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '01'): ?>selected="selected"<?php endif ?><?php endif ?> value="01">January</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '02'): ?>selected="selected"<?php endif ?><?php endif ?> value="02">February</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '03'): ?>selected="selected"<?php endif ?><?php endif ?> value="03">March</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '04'): ?>selected="selected"<?php endif ?><?php endif ?> value="04">April</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '05'): ?>selected="selected"<?php endif ?><?php endif ?> value="05">May</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '06'): ?>selected="selected"<?php endif ?><?php endif ?> value="06">June</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '07'): ?>selected="selected"<?php endif ?><?php endif ?> value="07">July</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '08'): ?>selected="selected"<?php endif ?><?php endif ?> value="08">August</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '09'): ?>selected="selected"<?php endif ?><?php endif ?> value="09">September</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '10'): ?>selected="selected"<?php endif ?><?php endif ?> value="10">October</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '11'): ?>selected="selected"<?php endif ?><?php endif ?> value="11">November</option>
                    <option <?php if (isset($_POST['month'])): ?><?php if ($_POST['month']== '12'): ?>selected="selected"<?php endif ?><?php endif ?> value="12">December</option>
                    <?php endif ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Week</label>
                  <select class="form-control" id="week" name="week">
                  <option value="" selected>-- Change Week --</option>
                  <?php if (isset($_POST['month'])): ?>
                    <?php foreach($weeks as $key => $value) {?>
                      <option <?php if (isset($_POST['week'])): ?><?php if ($_POST['week']== $value['value']): ?>selected="selected"<?php endif ?><?php endif ?> value="<?php echo $value['value'] ?>"><?php echo $value['week'] ?></option>
                      <?php }?>
                    <?php endif ?>
                  </select>
                </div>
                <button name="view" type="submit" class="btn btn-block btn-success">View Chart</button>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
                  
                
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
     
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <?php if (isset($_POST['week'])):?>
    <script>
      let barChartData = {
			labels: [
        <?php foreach ($week as $key => $value) :?>
                <?php $date=date_create($value); echo "'".date_format($date,"d-m-Y")."',"; ?>
          <?php endforeach ?>
      ],
			datasets: [{
				label: 'Pending',
				backgroundColor: window.chartColors.yellow,
				data: [
          <?php foreach ($week as $key => $value) :?>
                <?php echo count(execQuery("SELECT * FROM orders WHERE status=0 AND created_at LIKE '$value%'")).","; ?>
          <?php endforeach ?>
				]
			}, {
				label: 'Shipping',
				backgroundColor: window.chartColors.blue,
				data: [
					<?php foreach ($week as $key => $value) :?>
                <?php echo count(execQuery("SELECT * FROM orders WHERE status=1 AND created_at LIKE '$value%'")).","; ?>
          <?php endforeach ?>
				]
			}, {
				label: 'Delivered',
				backgroundColor: window.chartColors.green,
				data: [
					<?php foreach ($week as $key => $value) :?>
                <?php echo count(execQuery("SELECT * FROM orders WHERE status=2 AND created_at LIKE '$value%'")).","; ?>
          <?php endforeach ?>
				]
      },
      {
				label: 'Cancelled',
				backgroundColor: window.chartColors.red,
				data: [
					<?php foreach ($week as $key => $value) :?>
                <?php echo count(execQuery("SELECT * FROM orders WHERE status=3 AND created_at LIKE '$value%'")).","; ?>
          <?php endforeach ?>
				]
			}
    ]

		};
		window.onload = function() {
			let ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: '<?php echo $start." - ".$end ?>'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};
    </script>
<?php endif ?>

<?php if (isset($_POST['month'])): $date = $_POST['year']."-".$_POST['month'];$first_date = date("d",strtotime(date("Y-m-d", strtotime($date)) . ", first day of this month"));$last_date = date("d",strtotime(date("Y-m-d", strtotime($date)) . ", last day of this month"));?>
    <script>
      let barChartData = {
			labels: [
        <?php for($i = 1; $i<=$last_date; $i++) { if($i < 10){$i="0".$i;}; $month = $_POST['month']; $year = $_POST['year']; echo "'$i-$month-$year',"?>
         <?php }?>
      ],
			datasets: [{
				label: 'Pending',
				backgroundColor: window.chartColors.yellow,
				data: [
					<?php for($i = 1; $i<=$last_date; $i++) { if($i < 10){$i="0".$i;}; $month = $_POST['month']; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=0 AND created_at LIKE '$year-$month-$i%'")).","?>
                      <?php }?>
				]
			}, {
				label: 'Shipping',
				backgroundColor: window.chartColors.blue,
				data: [
					<?php for($i = 1; $i<=$last_date; $i++) { if($i < 10){$i="0".$i;}; $month = $_POST['month']; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=1 AND created_at LIKE '$year-$month-$i%'")).","?>
                      <?php }?>
				]
			}, {
				label: 'Delivered',
				backgroundColor: window.chartColors.green,
				data: [
					<?php for($i = 1; $i<=$last_date; $i++) { if($i < 10){$i="0".$i;}; $month = $_POST['month']; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=2 AND created_at LIKE '$year-$month-$i%'")).","?>
                      <?php }?>
				]
      },
      {
				label: 'Cancelled',
				backgroundColor: window.chartColors.red,
				data: [
					<?php for($i = 1; $i<=$last_date; $i++) { if($i < 10){$i="0".$i;}; $month = $_POST['month']; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=3 AND created_at LIKE '$year-$month-$i%'")).","?>
                      <?php }?>
				]
			}
    ]

		};
		window.onload = function() {
			let ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: '<?php $monthNum  = $_POST['month'];$dateObj   = DateTime::createFromFormat('!m', $monthNum); echo $dateObj->format('F')." ".$_POST['year'] ?>'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};
    </script>
<?php endif ?>
<?php if (isset($_POST['year'])): ?>
    <script>
      let barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October','November','December'],
			datasets: [{
				label: 'Pending',
				backgroundColor: window.chartColors.yellow,
				data: [
					<?php for($i = 1; $i<=12; $i++) { if($i < 10){$i="0".$i;}; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=0 AND created_at LIKE '$year-$i%'")).","?>
                      <?php }?>
				]
			}, {
				label: 'Shipping',
				backgroundColor: window.chartColors.blue,
				data: [
					<?php for($i = 1; $i<=12; $i++) { if($i < 10){$i="0".$i;}; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=1 AND created_at LIKE '$year-$i%'")).","?>
                      <?php }?>
				]
			}, {
				label: 'Delivered',
				backgroundColor: window.chartColors.green,
				data: [
					<?php for($i = 1; $i<=12; $i++) { if($i < 10){$i="0".$i;}; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=2 AND created_at LIKE '$year-$i%'")).","?>
                      <?php }?>
				]
      },
      {
				label: 'Cancelled',
				backgroundColor: window.chartColors.red,
				data: [
					<?php for($i = 1; $i<=12; $i++) { if($i < 10){$i="0".$i;}; $year = $_POST['year']; echo count(execQuery("SELECT * FROM orders WHERE status=3 AND created_at LIKE '$year-$i%'")).","?>
                      <?php }?>
				]
			}
    ]

		};
		window.onload = function() {
			let ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: '<?php echo "Year ".$_POST['year'] ?>'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};
    </script>
<?php endif ?>
<script>
		let barChartData = {
			labels: [
        <?php foreach ($date as $key => $value) :?>
                <?php echo "'".$value."',"; ?>
              <?php endforeach ?>	
      ],
			datasets: [{
				label: 'Pending',
				backgroundColor: window.chartColors.yellow,
				data: [
					<?php foreach ($date_sql as $key => $value) :?>
                      <?php echo count(execQuery("SELECT * FROM orders WHERE status=0 AND created_at LIKE '$value%'")).","; ?>
                  <?php endforeach ?>
				]
			}, {
				label: 'Shipping',
				backgroundColor: window.chartColors.blue,
				data: [
					<?php foreach ($date_sql as $key => $value) :?>
                      <?php echo count(execQuery("SELECT * FROM orders WHERE status=1 AND created_at LIKE '$value%'")).","; ?>
                  <?php endforeach ?>
				]
			}, {
				label: 'Delivered',
				backgroundColor: window.chartColors.green,
				data: [
					<?php foreach ($date_sql as $key => $value) :?>
                      <?php echo count(execQuery("SELECT * FROM orders WHERE status=2 AND created_at LIKE '$value%'")).","; ?>
                  <?php endforeach ?>
				]
      },
      {
				label: 'Cancelled',
				backgroundColor: window.chartColors.red,
				data: [
					<?php foreach ($date_sql as $key => $value) :?>
                      <?php echo count(execQuery("SELECT * FROM orders WHERE status=3 AND created_at LIKE '$value%'")).","; ?>
                  <?php endforeach ?>
				]
			}
    ]

		};
		window.onload = function() {
			let ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: '<?php $day = date('w'); echo date('j  F Y', strtotime('-'.($day-1).' days'))." - ".date('j F Y', strtotime('+'.(7-$day).' days')); ?>'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};
</script>
<?php
  // if(isset($_SESSION['login_admin'])){
  //   echo("<script> swal({title: 'login success',text: ' ',icon: 'success',timer: 2000,buttons: false,})</script>"); 
  //   unset($_SESSION['login_admin']);
  // }
  include 'footer.php';
?>
