<?php
include '../connect.php';
if(isset($_GET['id'])){
	$id = $_GET['id'];
	$edit_sts = "SELECT * FROM styles WHERE id = '$id'";
	$edit_style = execQuery($edit_sts);
	if(isset($_POST['name'])){
		$name = $_POST['name'];
		$update_sts = "UPDATE styles SET name = '$name' WHERE id = '$id'";
		$update_style = execQuery($update_sts);
		header('location: category_style.php');
	}
include 'header.php';
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Style</h3>
				</div>
				<div class="panel-body">
					<form action="" method="POST" role="form">
						<div class="form-group">
							<label for="">Edit Style</label>
							<input type="text" class="form-control" id="" placeholder="Input field" name="name" value="<?php echo $edit_style[0]["name"]?>">
						</div>
						<button type="submit" class="btn btn-success">Edit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  
include 'footer.php';
?>