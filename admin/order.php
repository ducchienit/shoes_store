<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$row = execQuery("SELECT * FROM orders");
$total = count($row);
$limit = 6;
$page = ceil($total/$limit);
$page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
$start = ($page_cr - 1)*$limit;
$sql_order = execQuery("SELECT orders.*, customers.full_name, customers.avatar
    FROM orders 
    LEFT JOIN customers ON orders.account_id = customers.account_id 
    ORDER BY orders.id LIMIT $start,$limit");
if(isset($_POST['nhap'])){
    header('location: order?search='.$_POST['nhap']);
}
if(isset($_GET['order_day'])){
    $date = $_GET['order_day'];
    $sql_order = execQuery("SELECT orders.*, customers.full_name, customers.avatar
    FROM orders
    LEFT JOIN customers ON orders.account_id = customers.account_id 
    WHERE orders.created_at LIKE '$date%' AND orders.status = 0 ORDER BY orders.id");
}
if(isset($_GET['search'])){
    if($_GET['search'] == ""){
        header('location: order');
    }
    $name_search = str_replace("'","\'",$_GET['search']);
    $sql_order = execQuery("SELECT orders.*, customers.full_name, customers.avatar
        FROM orders 
        LEFT JOIN customers ON orders.account_id = customers.account_id 
        WHERE (full_name LIKE '%$name_search%' OR orders.address LIKE '%$name_search%') ORDER BY orders.id");
    $_SESSION['search'] = $_GET['search'];
}
if(isset($_GET['status'])){
    if($_GET['status'] == "pending"){
        $status = 0;  
    }
    if($_GET['status'] == "shipping"){
        $status = 1;  
    }
    if($_GET['status'] == "delivered"){
        $status = 2;  
    }
    if($_GET['status'] == "cancelled"){
        $status = 3;  
    }
    $row = execQuery("SELECT orders.*, customers.full_name, customers.avatar
        FROM orders 
        LEFT JOIN customers ON orders.account_id = customers.account_id 
        WHERE status = '$status' ORDER BY orders.id");
    $total = count($row);
    $limit = 6;
    $page = ceil($total/$limit);
    $page_cr = (isset($_GET['page']))? $_GET['page'] : 1;
    $start = ($page_cr - 1)*$limit;
    $sql_order = execQuery("SELECT orders.*, customers.full_name, customers.avatar
        FROM orders 
        LEFT JOIN customers ON orders.account_id = customers.account_id 
        WHERE status = '$status' ORDER BY orders.id LIMIT $start,$limit");
}
if(isset($_POST['view'])){
    $id = $_POST['view'];
    $sql_order_detail = execQuery("SELECT order_detail.*, 
    sizes.size as size, 
    colors.name as color, 
    products.name as name_product,
    products.image as image_product,
    orders.total_price as total_price,
    orders.status as status
    FROM order_detail 
    INNER JOIN product_sizes ON order_detail.product_size_id = product_sizes.id 
    INNER JOIN sizes ON product_sizes.size_id = sizes.id 
    INNER JOIN product_colors ON order_detail.product_color_id = product_colors.id 
    INNER JOIN colors ON product_colors.color_id = colors.id 
    INNER JOIN products ON product_sizes.product_id = products.id
    INNER JOIN orders ON order_detail.order_id = orders.id 
    WHERE order_detail.order_id = '$id' ORDER BY order_detail.id");
    if($sql_order_detail == null){
        $not_insert = "";
        $sql_order_detail = execQuery("SELECT * FROM orders WHERE id = '$id'");
    }
}
if(isset($_POST['update'])){
    $id = $_POST['update'];
    $status = $_POST['status'];
    execQuery("UPDATE orders SET status = '$status' WHERE id = '$id'");
    $_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Update Status Success',
        showConfirmButton: false,
        timer: 1500
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">List Orders</h3>
				</div>
                <form style="margin: 10px 0px;" class="col-md-6" method="POST" class="navbar-form navbar-left" role="search" enctype="multipart/form-data" style="margin-left: 0px;margin-right: 0px;">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="search" name="search" type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                            </span>
                            <input id="form-search" type="text" value="<?php if (isset($_SESSION['search'])): ?><?php echo $_SESSION['search'] ?><?php endif ?>" name="nhap" class="form-control" placeholder="Search Customer or Address">
                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                </form>
                <div class="btn-group col-md-6 row">
                    <?php if (! isset($_GET['search']) && !isset($_GET['order_day'])): ?>
                        <div class="col-md-12" style="margin: 10px 0px;">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            View Follow Status <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                                <li><a href="?status=pending">Pending</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="?status=shipping">Shipping</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="?status=delivered">Delivered</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="?status=cancelled">Cancelled</a></li>
                                <li role="separator" class="divider"></li>                                     
                        </ul>
                        </div>
                    <?php endif ?>
                </div>
				<div class="panel-body">
					<table class="table table-bordered table-hover">
						<thead>
							<tr >
								<th class="text-center">STT</th>
								<th class="text-center">Customer</th>
								<th class="text-center">Address</th>
                                <th class="text-center">Order Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Net Amount</th>
                                <th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
                        <?php foreach($sql_order as $key=>$value) {?>	
							<tr>
								<td style="padding: 21px 0px;" class="text-center"><?php echo $key + 1 ?></td>
								<td class="text-center">
                                <div style="float: left;">
                                    <a style="color: #252525;" title="<?php echo $value['full_name'] ?>" href="account?search=<?php echo $value['full_name'] ?>">
                                    <img alt="Overview" src="../image/<?php if($value['avatar'] != NULL)  echo $value['avatar']?><?php else echo("default.png") ?>" align="middle" border="0" height="50" width="50" style="border-radius: 50%;box-shadow: 0px 0px 1px;">
                                    <span style="margin: 0px 0px 0px 10px;" class="iconLabel"><?php echo $value['full_name'] ?></span>
                                    </a>
                                </div>
                                </td>
                                <td style="padding: 21px 0px;" class="text-center"><?php echo $value['address'] ?></td>
                                <td style="padding: 21px 0px;" class="text-center"><?php echo date("d-m-Y H:i:s", strtotime($value['created_at'])); ?></td>
                                <td style="padding: 21px 0px;" class="text-center">
                                    <?php if ($value['status']==0) {?><span class="label label-warning">Pending</span><?php  }?>
                                    <?php if ($value['status']==1) {?><span class="label label-info">Shipping</span><?php  }?>
                                    <?php if ($value['status']==2) {?><span class="label label-success">Delivered</span><?php  }?>
                                    <?php if ($value['status']==3) {?><span class="label label-danger">Cancelled</span><?php  }?>
                                </td>
                                <td style="padding: 21px 0px;" class="text-center"><?php echo number_format($value['total_price'],"0",",",".")." VND" ?></td>
								<td style="padding: 21px 0px;" class="text-center">
                  					<form action="" method="POST" id="remove">
                                        <button type="submit" name="view" value="<?php echo $value['id'] ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Detail</button>  
                                    </form>
								</td>
							</tr>
                        <?php }?>
						</tbody>
					</table>
               
                            <?php if (! isset($_GET['search']) && !isset($_GET['order_day'])): ?>
                                <?php if ($page > 1): ?>
                                    <div style="text-align: right;">
                                        <ul class="pagination" style="margin: 0px;">
                                            <?php if ($page_cr>1) {?>
                                                <li>
                                                    <a href="order?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $page_cr - 1 ?>">&laquo;</a>
                                                </li>
                                            <?php } ?>
                                            <?php for ($i = 1; $i <= $page ; $i++) {?>
                                            <li id="page<?php echo $i ?>">
                                                <a href="order?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $i ?>"><?php echo $i ?></a>
                                            </li>
                                            <?php } ?>
                                            <?php if ($page_cr<$page) {?>
                                            <li>
                                                <a href="order?<?php if (isset($_GET['status'])): ?><?php echo("status=".$_GET['status']."&") ?><?php endif ?>page=<?php echo $page_cr + 1 ?>">&raquo;</a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php endif ?>
                            <?php endif ?>
                        
				</div>
			</div>
        </div>
	</div>
</section>
<div class="modal fade" id="view" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>"  class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>
        <h4 class="modal-title">List Order Detail</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
			<thead>
                <tr >
                    <th class="text-center">STT</th>
                    <th class="text-center">Image</th>
					<th class="text-center">Name</th>
					<th class="text-center">Size</th>
                    <th class="text-center">Color</th>
                    <th class="text-center">Price</th>
                    <th class="text-center">Quantity</th>
				</tr>
			</thead>
			<tbody>
                <?php if(! isset($not_insert)) {?>
                    <?php foreach($sql_order_detail as $key=>$value) {?>	
                        <tr>
                            <td class="text-center"><?php echo $key + 1 ?></td>
                            <td class="text-center">
                                <img style="height: 50px;" src="../image/<?php echo($value['image_product'])?>" alt="">                               
                            </td>
                            <td class="text-center"><?php echo $value['name_product'] ?></td>
                            <td class="text-center"><?php echo $value['size'] ?></td>
                            <td class="text-center"><?php echo $value['color'] ?></td>
                            <td class="text-center"><?php echo number_format($value['price'],"0",",",".")." VND" ?></td>
                            <td class="text-center"><?php echo $value['quantity'] ?></td>
                        </tr>
                    <?php }?>
                <?php }?>     
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group" style="">
                <label for="">Status</label>
                <form action="" method="POST">
                <div class="input-group">
                    <select class="form-control" <?php if($sql_order_detail[0]['status']==2) {?><?php echo ("disabled") ?><?php }?> name="status" required>
                        <?php if($sql_order_detail[0]['status']==3) {?>
                            <option <?php if ($sql_order_detail[0]['status']==0) {?>selected="selected"<?php }?>value="0">Pending</option>
                        <?php }?>
                        <?php if($sql_order_detail[0]['status']!=2 && $sql_order_detail[0]['status']!=3) {?>
                            <option <?php if ($sql_order_detail[0]['status']==1) {?>selected="selected"<?php }?>value="1">Shipping</option>
                        <?php }?>
                        <?php if($sql_order_detail[0]['status']==2 || $sql_order_detail[0]['status']==1) {?>
                            <option <?php if ($sql_order_detail[0]['status']==2) {?>selected="selected"<?php }?> value="2">Delivered</option>
                        <?php }?>
                        <?php if($sql_order_detail[0]['status']!=2 && $sql_order_detail[0]['status']!=1) {?>
                            <option <?php if ($sql_order_detail[0]['status']==3) {?>selected="selected"<?php }?>value="3">Cancelled</option>
                        <?php }?>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" <?php if($sql_order_detail[0]['status']==2) {?><?php echo ("disabled") ?><?php }?> name="update" value="<?php if (isset($_POST['view'])): ?><?php if(! isset($not_insert))  echo $sql_order_detail[0]['order_id'] ?><?php else echo $sql_order_detail[0]['id'] ?><?php endif ?>" type="submit">Update</button>
                    </span>
                    </div><!-- /input-group -->
                </form>
            </div>
            </div>
            <div class="col-md-6" style="text-align: end;">
                    <h3>Total Price: <i><ins class="total"><?php echo number_format($sql_order_detail[0]['total_price'],"0",",",".")." VND" ?></ins></i></h3>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-default" >Close</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php if (isset($_POST['view'])): ?>
    <script>
        $(document).ready(function(){
            $('#view').modal('show');
        });
    </script>
<?php endif ?>
<script>
    $(document).ready(function(){
        $("#form-search").hide();
        $("#search").click(function(){
            $("#form-search").toggle("");
        });
    });
    function $_GET(param) {
        var vars = {};
        window.location.href.replace( location.hash, '' ).replace( 
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;	
        }
        return vars;
    }
    var name = $_GET('page');
    if(name == "null"){
        document.getElementById("page1").classList.add("active");
    }
    document.getElementById("page"+name).classList.add("active");
</script>
<?php
    include 'footer.php';
?>