<?php
include '../connect.php';
if(isset($_POST['name'])){
	$name = $_POST['name'];
	$color_cate = "INSERT INTO colors(name)VALUES('$name')";
	$color = execQuery($color_cate);
}
$select_color = "SELECT * FROM colors";
$select_colors = execQuery($select_color);
include 'header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
				</div>
				<div class="panel-body">
					<form action="" method="POST" role="form">
						<legend>Add color</legend>
						<div class="form-group">
							<input type="text" name="name" id="" placeholder="Enter color" class="form-control">
						</div>
						<button type="submit" class="btn btn-primary">Add</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Information Color</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th style="text-align: center">STT</th>
									<th style="text-align: center">Name color</th>
									<th style="text-align: center">Option</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($select_colors as $key => $value) : ?>
								<tr>
									<td style="text-align: center"><?php echo $key+1 ?></td>
									<td style="text-align: center"><?php echo $value["name"]?></td>
									<td style="text-align: center">
									<a href="edit_color.php?id=<?php echo $value['id']?>" class="btn btn-success">Edit</a>
									<a href="delete_color.php?id=<?php echo $value['id']?>" class="btn btn-danger">Delete</a>
									</td>
								</tr>
							<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  
include 'footer.php';
?>