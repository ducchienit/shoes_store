<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
$sql = "SELECT * FROM sizes ORDER BY sizes.size ASC"; 
$result = execQuery($sql);
if (isset($_POST['add'])) {
	$check = execQuery("SELECT * FROM sizes");
    $not_name = str_replace(" ","",$_POST['name']);
    $size_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['size']);
        array_push($size_array,$name_value);
	}
	if(in_array($not_name,$size_array)){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'This size already exists',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
  	$name = $_POST['name'];
  	$sql_insert = "INSERT INTO sizes (size) VALUES ('$name')";
  	execQuery($sql_insert);
  	$_SESSION['title'] = "Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Add New Size Success',
    })";
    return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST['edit'])){
	$id = $_POST['edit'];
    $sql_size = "SELECT * FROM sizes WHERE id = '$id' ";
    $edit =  execQuery($sql_size);
	$row = $edit[0];
}
if(isset($_POST['remove'])){
	$id = $_POST['remove'];
	$product = execQuery("SELECT * FROM product_sizes WHERE size_id ='$id'");
	if(count($product) > 0){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'Can not remove',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
	$delete = "DELETE FROM sizes WHERE id = '$id'";
	execQuery($delete);
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Deleted successfully',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
if(isset($_POST["update"])){
	$id = $_POST["update"];
	$check = execQuery("SELECT * FROM sizes EXCEPT SELECT * FROM sizes WHERE id = '$id'");
    $not_name = str_replace(" ","",$_POST['name']);
    $size_array = [];
    foreach ($check as $key => $value) {
        //if (strstr($string, $url)) { // mine version
        $name_value = str_replace(" ","",$value['size']);
        array_push($size_array,$name_value);
	}
	if(in_array($not_name,$size_array)){
		$_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'warning',
			title: 'This size already exists',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
    $name = $_POST['name'];
    $update = "UPDATE sizes SET size='$name' WHERE id = '$id'";
    execQuery($update);
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Update Size Success',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title" id="form-title">Add New Size</h3>
				</div>
				<div class="panel-body">
				<form action="" method="POST" role="form" id="form-cate">
				<div class="form-group">
					<label for="">Size: <b id="size"></b></label>
                    <input name="name"  type="range" min="35" max="48" class="slider" id="myRange" value="<?php if(isset($_POST['edit']))  echo $row['size'] ?><?php else echo("35") ?>" required>
				</div>

				<button id="submit" type="submit" value="<?php if (isset($_POST['edit'])): ?><?php echo $row['id'] ?><?php endif ?>" name="add" class="btn btn-primary">Add</button>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" style="display: none;" type="" id="cancel" name="cancel" class="btn btn-danger" onclick="cancel()">Cancel</a>
			</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">List Sizes</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered table-hover">
						<thead>
							<tr >
								<th style="text-align: center">STT</th>
								<th style="text-align: center">Size</th>
								<th style="text-align: center">Action</th>
							</tr>
						</thead>
						<tbody>
              				<?php for($i = 0; $i < count($result); $i++) {$row = $result[$i]?>
							<tr>
								<td style="text-align: center"><?php echo $i +1?></td>
								<td style="text-align: center"><label  style="font-size: 1.5rem;" for="" class="label label-info"><?php echo $row['size']?></label></td>
								<td style="text-align: center">
                  					<form action="" method="POST">
										<button type="submit" name="edit" value="<?php echo $row['id']?>" class="btn btn-success btn-xs">Edit</button>
										<button type="submit" name="remove" value="<?php echo $row['id']?>" class="btn btn-danger btn-xs">Remove</button>
									</form>
								</td>
							</tr>
              				<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if (isset($_POST['edit'])): ?>
	<script>
    document.getElementById("form-title").innerText = "Edit Size";
    document.getElementById("submit").setAttribute("name", "update");
    document.getElementById("submit").innerText = "Update";
    document.getElementById("form-cate").lastElementChild.style.display = "";
  </script>
<?php endif ?>
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("size");
    output.innerHTML = slider.value;
    slider.oninput = function() {
        output.innerHTML = this.value;
    }
</script>
<?php
    include 'footer.php';
?>