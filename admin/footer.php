</div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2018 <a>TTPM_BKAP</a>.</strong>
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->

<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/adminlte.min.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/function.js"></script>
<script src="js/sweetalert2@9.js"></script>
<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') {?>
  <script> 
    <?php echo $_SESSION['title'] ?>
  </script>
<?php unset($_SESSION['title']); }?>
<script>
    function formatNumberField() {
        // unformat the value
        let value = this.value.replace(/[^\d,]/g, '');

        // split value into (leading digits, 3*x digits, decimal part)
        // also allows numbers like ',5'; if you don't want that,
        // use /^(\d{1,3})((?:\d{3})*))((?:,\d*)?)$/ instead
        let matches = /^(?:(\d{1,3})?((?:\d{3})*))((?:,\d*)?)$/.exec(value);

        if (!matches) {
            // invalid format; deal with it however you want to
            // this just stops trying to reformat the value
            return;
        }

        // add a space before every group of three digits
        let spaceified = matches[2].replace(/(\d{3})/g,'.$1');

        // now splice it all back together
        this.value = [matches[1], spaceified, matches[3]].join('');
    }
    document.getElementById('price').onkeyup = formatNumberField;
    function formatNumber(numberString) {
            let commaIndex = numberString.indexOf(',');
            let int = numberString;
            let frac = '';

            if (~commaIndex) {
                int = numberString.slice(0, commaIndex);
                frac = ',' + numberString.slice(commaIndex + 1);
            }

            let firstSpanLength = int.length % 3;
            let firstSpan = int.slice(0, firstSpanLength);
            let result = [];

            if (firstSpan) {
                result.push(firstSpan);
            }

            int = int.slice(firstSpanLength);

            let restSpans = int.match(/\d{3}/g);

            if (restSpans) {
                result = result.concat(restSpans);
                return result.join('.') + frac;
            }

            return firstSpan + frac;
    }
    $('#price').val(formatNumber($('#price').val()));
    document.getElementById("image").addEventListener("change", myFunction);
    function myFunction(event) {
            let value = document.getElementById('image').value;
            if(value == ""){
                document.getElementById('img').src = "";
            }else{
                let reader = new FileReader();
                let output = document.getElementById('img');
                reader.onload = function(){
                    output.src = reader.result;
                }
                reader.readAsDataURL(event.target.files[0]);
            }
    }
    document.getElementById("images").addEventListener("change", images);
    function images(evt) {
        let value = document.getElementById('images').value;
        if(value != "" || value == ""){
            document.getElementById('list-images').innerHTML = " ";
        }
        let files = evt.target.files;

        // Loop through the FileList and render image files as thumbnails.
        for (let i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        let reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
            // Render thumbnail.
            let div = document.createElement('div');
            div.innerHTML = 
            [
                '<img style="width: 100%;height: 100px;" src="', 
                e.target.result,
                '" title="', escape(theFile.name), 
                '"/>'
            ].join('');
            
            document.getElementById('list-images').insertBefore(div, null);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        }
    }
    $(document).ready(function(){
        $("#form-search").hide();
        $("#search").click(function(){
            $("#form-search").toggle("");
        });
    });
    $(document).ready(function(){
        $("#close").click(function(){
            $('#Modal').modal('hide');
        });
    });
    function $_GET(param) {
        let vars = {};
        window.location.href.replace( location.hash, '' ).replace( 
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;	
        }
        return vars;
    }
    let page = $_GET('page');
    if(page == null){
        document.getElementById("page1").classList.add("active");
    }
    document.getElementById("page"+page).classList.add("active");
</script>
</body>
</html>