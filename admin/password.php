<?php
session_start();
include '../connect.php';
if(! isset($_SESSION['admin'])){
    return header("location: ../login.php");
}
if(isset($_SESSION['customer'])){
  return header("location: ../index.php");
}
if(isset($_POST['update'])){
	$admin = $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$admin'")[0];
    $password = md5($_POST['password']);
    $new_password = md5($_POST['new_password']);
	$confirm_password = md5($_POST['confirm_password']);
	$account_id = $info['account_id'];
    $account = execQuery("SELECT * FROM accounts  WHERE id = '$account_id'")[0];
    $password_account = $account['password'];
    if($password == $password_account && $new_password == $confirm_password){
        execQuery("UPDATE accounts SET password='$new_password' WHERE id = '$account_id'");
        $_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'success',
			title: 'Password Changed Successfully',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'error',
		title: 'Password Changed Error',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
include 'header.php'
?>
<section class="container" style="display: contents;">
	<div class="row" style="padding: 15px 15px;">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title" id="form-title">Change Password</h3>
				</div>
				<div class="panel-body">
				<form action="" method="POST" role="form" id="">
				<div class="form-group">
					<label for="">Current Password</label>
					<input type="password" class="form-control" id="new" placeholder="Input field" name="password" required>
				</div>
                <div class="form-group">
					<label for="">New Password</label>
					<input type="password" class="form-control" id="name" placeholder="Input field" name="new_password" required>
				</div>
                <div class="form-group">
					<label for="">Confirm password</label>
					<input type="password" class="form-control" id="name" placeholder="Input field" name="confirm_password" required>
				</div>
				<button id="submit" type="submit" name="update" class="btn btn-danger">Apply</button>
			</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
    include 'footer.php';
?>