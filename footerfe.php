	<div class="foote" style="box-shadow: 0px 0px 5px #252525;">
		<div class="container">
			<div class="row">
				<div class=" col-md-3 wow slideInDown">
					<div class="widget-ft">
						<h4 class="title-menu">
							<a role="button">
								Hệ thống cửa hàng
							</a>
						</h4>
						<div>
							<div class="list-menu">
								<div class="widget-ft wg-logo">													
									<div class="item">
										<h4 class="title-menu4 icon_none_first">
											<a>An thượng</a>
										</h4>
										<ul class="contact contact_x">
											<li>
												<span class="txt_content_child">
													<span>Địa chỉ:</span>
													Đào nguyên- An thượng
												</span>
											</li>										
											<li class="sdt">
												<span> Hotline:</span>
												<a href="tel:0989879000" style="text-decoration: none;list-style: none;color: #acacac">0123456789</a>
											</li>
										</ul>
									</div>		
									<div class="item">
										<h4 class="title-menu4 icon_none_first">
											<a>An thượng</a>
										</h4>
										<ul class="contact contact_x">		
											<li>
												<span class="txt_content_child">
													<span>Địa chỉ:</span>
													Đào nguyên- An thượng
												</span>
											</li>	
											<li class="sdt">
												<span> Hotline:</span>
												<a href="tel:0989879000" style="text-decoration: none;list-style: none;color: #acacac">0123456789</a>
											</li>				
										</ul>
									</div>					
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-3 wow slideInDown">
					<h4 class="title-menu">
						<a role="button">
							Hệ thống cửa hàng
						</a>
					</h4>
					<div class="c" id="" style="">
						<ul class="list-menu">

							<li class="li_menu"><a href="home.php">Trang chủ</a></li>

							<li class="li_menu"><a href="#">Thương hiệu</a></li>

							<li class="li_menu"><a href="#">Sản phẩm</a></li>

							<li class="li_menu"><a href="#">Danh mục</a></li>

							<li class="li_menu"><a href="#">Phong cách</a></li>

						</ul>
					</div>
				</div>

				<div class=" col-md-3 wow slideInDown">
					<h4 class="title-menu">
						<a role="button">
							Chính sách
						</a>
					</h4>
					<div class="c" id="" style="">
						<ul class="list-menu">

							<li class="li_menu"><a href="#">Trang chủ</a></li>

							<li class="li_menu"><a href="#">Thương hiệu</a></li>

							<li class="li_menu"><a href="#">Sản phẩm</a></li>

							<li class="li_menu"><a href="#">Danh mục</a></li>

							<li class="li_menu"><a href="#">Phong cách</a></li>

						</ul>
					</div>
				</div>

				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<div class="widget-ft wow slideInDown">
						<h4 class="title-menu">
							<a role="button">
								Đăng ký
							</a>
						</h4>
						<div class="footer-widget">
							<a href="/" class="bocongthuong">					
								<img src="//bizweb.dktcdn.net/100/369/492/themes/741260/assets/bocongthuong.png?1594607980046" alt="Sea Shoes">					
							</a>
						</div>
						<h4 class="title-menu tittle_time">
							<a role="button">
								Thanh toán
							</a>
						</h4>
						<div class="time_work">
							<ul class="list-menu">
								<li class="li_menu li_menu_xxx">
									<a href="/" class="bocongthuong">					
										<img src="//bizweb.dktcdn.net/100/369/492/themes/741260/assets/payment.png?1594607980046" alt="" width="190px">					
									</a>

								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- het footer -->
	<div class="loichao" style="background-color: black">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="text-align: center;">
					<p style="color: white;margin-top: 10px">Copyright © 2014 bangiay.com – All rights reserved</p>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.5.1.slim.min.js"></script>
	<script src="js/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="js/bootstrap.min.js" integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous"></script>
	
	<script src="OwlCarousel/dist/owl.carousel.min.js"></script>
	<script src="js/sweetalert2@9.js"></script>
	<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') {?>
		<script> 
			<?php echo $_SESSION['title'] ?>
		</script>
	<?php unset($_SESSION['title']); }?>
</body>
<script src="js_lib/wow.min.js"></script>
<script>
	$(document).ready(function(){
        $(".dropdown-info").hide();
        $(".info").click(function(){
            $(".dropdown-info").toggle("");
        });
    });
	$(document).ready(function(){
		$(document).on("click",'.searchmobile',function(){
			$("#searchbox").toggle();
		});
	});
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:20,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
	})
	new WOW().init();
</script>
</html>