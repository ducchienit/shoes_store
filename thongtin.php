<?php  
include 'menu.php';
if(! isset($_SESSION['customer']) && ! isset($_SESSION['admin'])){
	header("location: login.php");
}
if(isset($_SESSION['customer']) || isset($_SESSION['admin'])){
    $full_name = (isset($_SESSION['customer'])) ? $_SESSION['customer'] : $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$full_name'")[0];
}
if(isset($_POST['update'])){
	$id = $_POST['update'];
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$address = $_POST['address'];
	$email = $_POST['email'];
	$birth = $_POST['birth'];
	$gender = $_POST['gender'];
	if(isset($_FILES['image'])){
		$file = $_FILES['image'];
		$file_name = $file['name'];
	}
	execQuery("UPDATE accounts SET email='$email' WHERE id='$id'");
	if($file_name != null){
		execQuery("UPDATE customers SET full_name='$name', avatar='$file_name', phone='$phone', gender='$gender', birth='$birth', address='$address' WHERE account_id = '$id'");
		move_uploaded_file($file['tmp_name'],'image/'.$file_name);
	}else{
		execQuery("UPDATE customers SET full_name='$name', phone='$phone', gender='$gender', birth='$birth', address='$address' WHERE account_id = '$id'");
	}
	$_SESSION['customer'] = $name;
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Đã lưu thông tin',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);

}
?>
<div class="banner_noidung">
	<h4>Thông tin tài khoản</h4>
</div>

<section class="container" style="">
	<div class="row" style="margin: 40px 0px;">
        <div class="col-md-12">
            <div class="thumbnail row" style="border: 2px solid #ddd;">
                <form style="display: contents;" action="" method="POST" enctype= multipart/form-data>
                    <div class="col-md-3" style="text-align: center;background-color: #d4d4d46b;padding: 20px 0px;">
                        
                        <img id="avatar" style="box-shadow: 0px 0px 30px #ccc;margin: 15px 0px;height: 215px;width: 200px;border-radius: 50%;" src="image/<?php if(isset($info) && strlen($info['avatar']) > 0)  echo $info['avatar'] ?><?php else echo("default.png") ?>" alt="...">
                        <img id="avatar_upload" style="display: none;box-shadow: 0px 0px 30px #ccc;margin: 15px 0px;height: 215px;width: 200px;border-radius: 50%;" src="" alt="...">
                        <div>
                            <label class="custom-file-upload">
                                <input type="file" name="image" id="image" class="form-control" style="display: none;">
                                <a id="upload" name="upload_image" class="btn btn-secondary" role="">Chọn ảnh <i class="fas fa-image"></i></a>
                            </label>
                        </div>
                    </div>
                    <div class="caption col-md-9" style="padding: 20px 15px;">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label style="font-weight: 500;" for="">Họ và tên</label>
                                <input type="text" name="name" value="<?php echo $info['full_name'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label style="font-weight: 500;" for="">Email</label>
                                <input type="email" name="email" value="<?php echo $info['email'] ?>" class="form-control" id="" placeholder="" required>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label style="font-weight: 500;" for="">Số điện thoại</label>
                                <input type="text" name="phone" value="<?php echo $info['phone'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 25px;">
                                <label style="font-weight: 500;" for="">Ngày sinh</label>
                                <input type="date" name="birth" value="<?php echo $info['birth'] ?>" class="form-control" id="" placeholder="" >
                            </div>
                            <div class="form-group" style="display: table-cell;">
							<label style="font-weight: 500;" for="">Giới tính</label>
                                <div class="list-gender">
									<div class="gender-item">
										<input type="radio" class="gender" name="gender" id="male" value="0" <?php if ($info['gender']=="0") {?>checked="checked"<?php }?> required>
										<label for="gender" class="gender-label">Nam</label>
									</div>
									<div class="gender-item">
										<input type="radio" class="gender" name="gender" id="female" value="1" <?php if ($info['gender']=="1") {?>checked="checked"<?php }?> required>
										<label for="gender" class="gender-label">Nữ</label>
									</div>
								</div>
                            </div>
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label style="font-weight: 500;" for="">Địa chỉ</label>
                                <textarea class="form-control" id="address" name="address" rows="3"><?php echo $info['address'] ?></textarea>
                            </div>
                            
						</div>
                        
                        </div>
                        <button type="" name="update" value="<?php echo $info['account_id'] ?>" class="btn btn-primary">Lưu thông tin <i class="far fa-save"></i></button>
                        <a role="button" href="password.php" class="btn btn-danger">Đổi mật khẩu <i class="fas fa-key"></i></a>                    
                    </div>
                </form>
            </div>
        </div>
	</div>
</section>
<script>
    document.getElementById("image").addEventListener("change", myFunction);
    var img = "";
    function myFunction(event) {
        console.log(event);
        img = document.getElementById("image").value;
        var path = img.slice(12);
        if(img != ""){
            document.getElementById("avatar").style.display = "none";
            document.getElementById("avatar_upload").style.display = "";
            var reader = new FileReader();
            var output = document.getElementById('avatar_upload');
            reader.onload = function(){
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }else{
            document.getElementById("avatar").style.display = "";
            document.getElementById("avatar_upload").style.display = "none";
        }
    }
</script>
<?php  
include 'footerfe.php';
?>

