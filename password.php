<?php
include 'menu.php';
if(! isset($_SESSION['customer']) && ! isset($_SESSION['admin'])){
	header("location: login.php");
}
if(isset($_POST['update'])){
	$name = (isset($_SESSION['customer'])) ? $_SESSION['customer'] : $_SESSION['admin'];
	$info = execQuery("SELECT customers.*, accounts.email  
    FROM customers 
    INNER JOIN accounts ON customers.account_id = accounts.id
    WHERE customers.full_name = '$name'")[0];
    $password = md5($_POST['password']);
    $new_password = md5($_POST['new_password']);
	$confirm_password = md5($_POST['confirm_password']);
	$account_id = $info['account_id'];
    $account = execQuery("SELECT * FROM accounts  WHERE id = '$account_id'")[0];
    $password_account = $account['password'];
    if($password == $password_account && $new_password == $confirm_password){
        execQuery("UPDATE accounts SET password='$new_password' WHERE id = '$account_id'");
        $_SESSION['title'] = "Swal.fire({
			position: 'center',
			icon: 'success',
			title: 'Đã đổi mật khẩu',
		})";
		return header("location: ".$_SERVER['REQUEST_URI']);
	}
	$_SESSION['title'] = "Swal.fire({
		position: 'center',
		icon: 'error',
		title: 'Không thể đôi mật khẩu',
	})";
	return header("location: ".$_SERVER['REQUEST_URI']);
}
?>
<!-- đăng nhập -->
<div class="banner_noidung">
    <h4>Đổi mật khẩu</h4>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 taikhoandn" style="padding: 20px 10px;">
            <div class="dangnhap">
                <form action="" method="POST">
                    <p>Mật khẩu hiện tại</p>
                    <input type="password" name="password" placeholder="Nhập mật khẩu hiện tại" required>
                    <p>Mật khẩu mới</p>
                    <input type="password" name="new_password" placeholder="Nhập mật khẩu mới" required>
                    <p>Xác nhận mật khẩu</p>
                    <input type="password" name="confirm_password" placeholder="Nhập lại mật khẩu mới" required>
                    <input title="Đăng nhập" type="submit" name="update" value="Thay đổi mật khẩu">
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
include 'footerfe.php';
?>