<?php
include 'menu.php';
if(isset($_SESSION['admin'])){
    return header("location: admin/home");
}
if(isset($_SESSION['customer'])){
    return header("location: home.php");
}
if(isset($_POST['login'])){
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $sql = "SELECT * FROM accounts WHERE email = '$email' AND password = '$password'";
    $result = execQuery($sql);
    if(count($result) > 0){
        $row = $result[0];
        $id = $row['id'];
        $customer = execQuery("SELECT * FROM customers WHERE account_id = '$id'")[0];
        if($row['is_admin'] === "1"){
            $_SESSION['admin'] = $customer['full_name'];
            $_SESSION['avatar'] = $customer['avatar'];
        }
        else{
            $_SESSION['customer'] = $customer['full_name'];
        }
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Đăng nhập thành công',
            showConfirmButton: false,
            timer: 1500
        })";
        return header("location: login.php");
    }
    else{
        $_SESSION['title'] = "Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Lỗi đăng nhập',
            text: 'Thông tin đăng nhập không chính xác.',
        })";
        return header("location: ".$_SERVER['REQUEST_URI']);
    }
}
?>
<!-- đăng nhập -->
<div class="banner_noidung">
    <h4>Đăng nhập tài khoản</h4>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 taikhoandn">
            <div class="dangnhap">
                <h1 style="font-size: 30px;text-align: center;">Đăng nhập tài khoản</h1>
                <form action="" method="POST">
                    <p>Email</p>
                    <input type="taikhoan" name="email" placeholder="Nhập email" required>
                    <p>Mật khẩu</p>
                    <input type="password" name="password" placeholder="Nhập mật khẩu" required>
                    <input title="Đăng nhập" type="submit" name="login" value="Đăng nhập">
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
include 'footerfe.php';
?>